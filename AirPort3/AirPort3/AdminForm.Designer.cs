﻿
namespace AirPort3
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Position = new System.Windows.Forms.TextBox();
            this.AName = new System.Windows.Forms.TextBox();
            this.AFirstName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.PictureBox();
            this.ChangeUser = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Filter6 = new System.Windows.Forms.RadioButton();
            this.Filter5 = new System.Windows.Forms.RadioButton();
            this.Filter4 = new System.Windows.Forms.RadioButton();
            this.Filter3 = new System.Windows.Forms.RadioButton();
            this.Filter2 = new System.Windows.Forms.RadioButton();
            this.Filter1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.Search = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TTable = new System.Windows.Forms.RadioButton();
            this.RTable = new System.Windows.Forms.RadioButton();
            this.PosTable = new System.Windows.Forms.RadioButton();
            this.PLTable = new System.Windows.Forms.RadioButton();
            this.PTable = new System.Windows.Forms.RadioButton();
            this.GTable = new System.Windows.Forms.RadioButton();
            this.FTable = new System.Windows.Forms.RadioButton();
            this.ETable = new System.Windows.Forms.RadioButton();
            this.BPTable = new System.Windows.Forms.RadioButton();
            this.BTable = new System.Windows.Forms.RadioButton();
            this.APTable = new System.Windows.Forms.RadioButton();
            this.ACTable = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exit)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Position);
            this.panel1.Controls.Add(this.AName);
            this.panel1.Controls.Add(this.AFirstName);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Exit);
            this.panel1.Controls.Add(this.ChangeUser);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1157, 35);
            this.panel1.TabIndex = 0;
            // 
            // Position
            // 
            this.Position.Enabled = false;
            this.Position.Location = new System.Drawing.Point(768, 7);
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            this.Position.Size = new System.Drawing.Size(188, 20);
            this.Position.TabIndex = 7;
            // 
            // AName
            // 
            this.AName.Enabled = false;
            this.AName.Location = new System.Drawing.Point(438, 7);
            this.AName.Name = "AName";
            this.AName.ReadOnly = true;
            this.AName.Size = new System.Drawing.Size(188, 20);
            this.AName.TabIndex = 6;
            // 
            // AFirstName
            // 
            this.AFirstName.Enabled = false;
            this.AFirstName.Location = new System.Drawing.Point(74, 7);
            this.AFirstName.Name = "AFirstName";
            this.AFirstName.ReadOnly = true;
            this.AFirstName.Size = new System.Drawing.Size(188, 20);
            this.AFirstName.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(697, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Должность";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(403, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Фамилия";
            // 
            // Exit
            // 
            this.Exit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Exit.Location = new System.Drawing.Point(1125, 3);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(27, 27);
            this.Exit.TabIndex = 1;
            this.Exit.TabStop = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // ChangeUser
            // 
            this.ChangeUser.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ChangeUser.Location = new System.Drawing.Point(962, 3);
            this.ChangeUser.Name = "ChangeUser";
            this.ChangeUser.Size = new System.Drawing.Size(157, 27);
            this.ChangeUser.TabIndex = 0;
            this.ChangeUser.Text = "Сменить пользователя";
            this.ChangeUser.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.Filter6);
            this.panel2.Controls.Add(this.Filter5);
            this.panel2.Controls.Add(this.Filter4);
            this.panel2.Controls.Add(this.Filter3);
            this.panel2.Controls.Add(this.Filter2);
            this.panel2.Controls.Add(this.Filter1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.Search);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(0, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(969, 125);
            this.panel2.TabIndex = 1;
            // 
            // Filter6
            // 
            this.Filter6.AutoSize = true;
            this.Filter6.Location = new System.Drawing.Point(700, 102);
            this.Filter6.Name = "Filter6";
            this.Filter6.Size = new System.Drawing.Size(14, 13);
            this.Filter6.TabIndex = 8;
            this.Filter6.TabStop = true;
            this.Filter6.UseVisualStyleBackColor = true;
            // 
            // Filter5
            // 
            this.Filter5.AutoSize = true;
            this.Filter5.Location = new System.Drawing.Point(700, 79);
            this.Filter5.Name = "Filter5";
            this.Filter5.Size = new System.Drawing.Size(14, 13);
            this.Filter5.TabIndex = 7;
            this.Filter5.TabStop = true;
            this.Filter5.UseVisualStyleBackColor = true;
            // 
            // Filter4
            // 
            this.Filter4.AutoSize = true;
            this.Filter4.Location = new System.Drawing.Point(418, 102);
            this.Filter4.Name = "Filter4";
            this.Filter4.Size = new System.Drawing.Size(14, 13);
            this.Filter4.TabIndex = 6;
            this.Filter4.TabStop = true;
            this.Filter4.UseVisualStyleBackColor = true;
            // 
            // Filter3
            // 
            this.Filter3.AutoSize = true;
            this.Filter3.Location = new System.Drawing.Point(418, 79);
            this.Filter3.Name = "Filter3";
            this.Filter3.Size = new System.Drawing.Size(14, 13);
            this.Filter3.TabIndex = 5;
            this.Filter3.TabStop = true;
            this.Filter3.UseVisualStyleBackColor = true;
            // 
            // Filter2
            // 
            this.Filter2.AutoSize = true;
            this.Filter2.Location = new System.Drawing.Point(7, 101);
            this.Filter2.Name = "Filter2";
            this.Filter2.Size = new System.Drawing.Size(14, 13);
            this.Filter2.TabIndex = 4;
            this.Filter2.TabStop = true;
            this.Filter2.UseVisualStyleBackColor = true;
            // 
            // Filter1
            // 
            this.Filter1.AutoSize = true;
            this.Filter1.Location = new System.Drawing.Point(8, 82);
            this.Filter1.Name = "Filter1";
            this.Filter1.Size = new System.Drawing.Size(14, 13);
            this.Filter1.TabIndex = 3;
            this.Filter1.TabStop = true;
            this.Filter1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(4, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Фильтр";
            // 
            // Search
            // 
            this.Search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Search.Location = new System.Drawing.Point(4, 30);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(960, 19);
            this.Search.TabIndex = 1;
            this.Search.TextChanged += new System.EventHandler(this.Search_TextChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Поиск";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.DeleteButton);
            this.panel3.Controls.Add(this.UpdateButton);
            this.panel3.Controls.Add(this.AddButton);
            this.panel3.Location = new System.Drawing.Point(975, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(182, 125);
            this.panel3.TabIndex = 2;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(9, 79);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(164, 23);
            this.DeleteButton.TabIndex = 2;
            this.DeleteButton.Text = "Удалить";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(9, 41);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(164, 23);
            this.UpdateButton.TabIndex = 1;
            this.UpdateButton.Text = "Изменить";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click_1);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(9, 3);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(164, 23);
            this.AddButton.TabIndex = 0;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.TTable);
            this.panel4.Controls.Add(this.RTable);
            this.panel4.Controls.Add(this.PosTable);
            this.panel4.Controls.Add(this.PLTable);
            this.panel4.Controls.Add(this.PTable);
            this.panel4.Controls.Add(this.GTable);
            this.panel4.Controls.Add(this.FTable);
            this.panel4.Controls.Add(this.ETable);
            this.panel4.Controls.Add(this.BPTable);
            this.panel4.Controls.Add(this.BTable);
            this.panel4.Controls.Add(this.APTable);
            this.panel4.Controls.Add(this.ACTable);
            this.panel4.Location = new System.Drawing.Point(0, 41);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1157, 28);
            this.panel4.TabIndex = 3;
            // 
            // TTable
            // 
            this.TTable.AutoSize = true;
            this.TTable.Location = new System.Drawing.Point(974, 6);
            this.TTable.Name = "TTable";
            this.TTable.Size = new System.Drawing.Size(63, 17);
            this.TTable.TabIndex = 11;
            this.TTable.TabStop = true;
            this.TTable.Text = "Билеты";
            this.TTable.UseVisualStyleBackColor = true;
            this.TTable.CheckedChanged += new System.EventHandler(this.TTable_CheckedChanged);
            // 
            // RTable
            // 
            this.RTable.AutoSize = true;
            this.RTable.Location = new System.Drawing.Point(859, 6);
            this.RTable.Name = "RTable";
            this.RTable.Size = new System.Drawing.Size(109, 17);
            this.RTable.TabIndex = 10;
            this.RTable.TabStop = true;
            this.RTable.Text = "Багажный талон";
            this.RTable.UseVisualStyleBackColor = true;
            this.RTable.CheckedChanged += new System.EventHandler(this.RTable_CheckedChanged);
            // 
            // PosTable
            // 
            this.PosTable.AutoSize = true;
            this.PosTable.Location = new System.Drawing.Point(770, 6);
            this.PosTable.Name = "PosTable";
            this.PosTable.Size = new System.Drawing.Size(83, 17);
            this.PosTable.TabIndex = 9;
            this.PosTable.TabStop = true;
            this.PosTable.Text = "Должности";
            this.PosTable.UseVisualStyleBackColor = true;
            this.PosTable.CheckedChanged += new System.EventHandler(this.PosTable_CheckedChanged);
            // 
            // PLTable
            // 
            this.PLTable.AutoSize = true;
            this.PLTable.Location = new System.Drawing.Point(687, 6);
            this.PLTable.Name = "PLTable";
            this.PLTable.Size = new System.Drawing.Size(77, 17);
            this.PLTable.TabIndex = 8;
            this.PLTable.TabStop = true;
            this.PLTable.Text = "Самолеты";
            this.PLTable.UseVisualStyleBackColor = true;
            this.PLTable.CheckedChanged += new System.EventHandler(this.PLTable_CheckedChanged);
            // 
            // PTable
            // 
            this.PTable.AutoSize = true;
            this.PTable.Location = new System.Drawing.Point(596, 4);
            this.PTable.Name = "PTable";
            this.PTable.Size = new System.Drawing.Size(85, 17);
            this.PTable.TabIndex = 7;
            this.PTable.TabStop = true;
            this.PTable.Text = "Пассажиры";
            this.PTable.UseVisualStyleBackColor = true;
            this.PTable.CheckedChanged += new System.EventHandler(this.PTable_CheckedChanged);
            // 
            // GTable
            // 
            this.GTable.AutoSize = true;
            this.GTable.Location = new System.Drawing.Point(545, 4);
            this.GTable.Name = "GTable";
            this.GTable.Size = new System.Drawing.Size(45, 17);
            this.GTable.TabIndex = 6;
            this.GTable.TabStop = true;
            this.GTable.Text = "Пол";
            this.GTable.UseVisualStyleBackColor = true;
            this.GTable.CheckedChanged += new System.EventHandler(this.GTable_CheckedChanged);
            // 
            // FTable
            // 
            this.FTable.AutoSize = true;
            this.FTable.Location = new System.Drawing.Point(481, 4);
            this.FTable.Name = "FTable";
            this.FTable.Size = new System.Drawing.Size(58, 17);
            this.FTable.TabIndex = 5;
            this.FTable.TabStop = true;
            this.FTable.Text = "Рейсы";
            this.FTable.UseVisualStyleBackColor = true;
            this.FTable.CheckedChanged += new System.EventHandler(this.FTable_CheckedChanged);
            // 
            // ETable
            // 
            this.ETable.AutoSize = true;
            this.ETable.Location = new System.Drawing.Point(391, 4);
            this.ETable.Name = "ETable";
            this.ETable.Size = new System.Drawing.Size(84, 17);
            this.ETable.TabIndex = 4;
            this.ETable.TabStop = true;
            this.ETable.Text = "Сотрудники";
            this.ETable.UseVisualStyleBackColor = true;
            this.ETable.CheckedChanged += new System.EventHandler(this.ETable_CheckedChanged);
            // 
            // BPTable
            // 
            this.BPTable.AutoSize = true;
            this.BPTable.Location = new System.Drawing.Point(272, 4);
            this.BPTable.Name = "BPTable";
            this.BPTable.Size = new System.Drawing.Size(120, 17);
            this.BPTable.TabIndex = 3;
            this.BPTable.TabStop = true;
            this.BPTable.Text = "Посадочный талон";
            this.BPTable.UseVisualStyleBackColor = true;
            this.BPTable.CheckedChanged += new System.EventHandler(this.BPTable_CheckedChanged);
            // 
            // BTable
            // 
            this.BTable.AutoSize = true;
            this.BTable.Location = new System.Drawing.Point(209, 3);
            this.BTable.Name = "BTable";
            this.BTable.Size = new System.Drawing.Size(57, 17);
            this.BTable.TabIndex = 2;
            this.BTable.TabStop = true;
            this.BTable.Text = "Багаж";
            this.BTable.UseVisualStyleBackColor = true;
            this.BTable.CheckedChanged += new System.EventHandler(this.BTable_CheckedChanged);
            // 
            // APTable
            // 
            this.APTable.AutoSize = true;
            this.APTable.Location = new System.Drawing.Point(118, 4);
            this.APTable.Name = "APTable";
            this.APTable.Size = new System.Drawing.Size(81, 17);
            this.APTable.TabIndex = 1;
            this.APTable.TabStop = true;
            this.APTable.Text = "Аэропорты";
            this.APTable.UseVisualStyleBackColor = true;
            this.APTable.CheckedChanged += new System.EventHandler(this.APTable_CheckedChanged);
            // 
            // ACTable
            // 
            this.ACTable.AutoSize = true;
            this.ACTable.Location = new System.Drawing.Point(12, 4);
            this.ACTable.Name = "ACTable";
            this.ACTable.Size = new System.Drawing.Size(100, 17);
            this.ACTable.TabIndex = 0;
            this.ACTable.TabStop = true;
            this.ACTable.Text = "Аэрокомпании";
            this.ACTable.UseVisualStyleBackColor = true;
            this.ACTable.CheckedChanged += new System.EventHandler(this.ACTable_CheckedChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 206);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1157, 482);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 688);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminForm";
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exit)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton TTable;
        private System.Windows.Forms.RadioButton RTable;
        private System.Windows.Forms.RadioButton PosTable;
        private System.Windows.Forms.RadioButton PLTable;
        private System.Windows.Forms.RadioButton PTable;
        private System.Windows.Forms.RadioButton GTable;
        private System.Windows.Forms.RadioButton FTable;
        private System.Windows.Forms.RadioButton ETable;
        private System.Windows.Forms.RadioButton BPTable;
        private System.Windows.Forms.RadioButton BTable;
        private System.Windows.Forms.RadioButton APTable;
        private System.Windows.Forms.RadioButton ACTable;
        private System.Windows.Forms.PictureBox Exit;
        private System.Windows.Forms.Button ChangeUser;
        private System.Windows.Forms.RadioButton Filter6;
        private System.Windows.Forms.RadioButton Filter5;
        private System.Windows.Forms.RadioButton Filter4;
        private System.Windows.Forms.RadioButton Filter3;
        private System.Windows.Forms.RadioButton Filter2;
        private System.Windows.Forms.RadioButton Filter1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.TextBox Position;
        public System.Windows.Forms.TextBox AName;
        public System.Windows.Forms.TextBox AFirstName;
    }
}