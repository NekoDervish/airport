﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AirPort3
{
    public partial class LoginForm : Form
    {
        public DataSet ds;
        public SqlDataAdapter adapter;
        public SqlCommandBuilder commandBuilder;
        public string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Airport.mdf;Integrated Security=True;Context Connection=False;TransparentNetworkIPResolution=False";

        

        public LoginForm()
        {
            InitializeComponent();
            Password.PasswordChar = '*';
        }

        private void OK_Click(object sender, EventArgs e)
        {
            
            string position = "";

            string firstName = "";
            string name = "";

            string sql = "SELECT EPos " +
                "FROM Employees " +
                "WHERE ELogin = '" + Login.Text + "' AND EPassword = '" + Password.Text + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand com = new SqlCommand(sql, connection);
                if (com.ExecuteScalar() != null)
                    position = com.ExecuteScalar().ToString();
            }

            if (position == "Администратор" && position != "")
            {
                string sql1 = "SELECT EFirstname " +
                "FROM Employees " +
                "WHERE ELogin = '" + Login.Text + "' AND EPassword = '" + Password.Text + "'";

                string sql2 = "SELECT EName " +
                "FROM Employees " +
                "WHERE ELogin = '" + Login.Text + "' AND EPassword = '" + Password.Text + "'";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql1, connection);
                    firstName = com.ExecuteScalar().ToString();

                    SqlCommand com1 = new SqlCommand(sql2, connection);
                    name = com1.ExecuteScalar().ToString();

                }

                AdminForm admin;
                admin = new AdminForm();
                admin.AFirstName.Text = firstName;
                admin.AName.Text = name;
                admin.Position.Text = position;

                admin.ShowDialog();
                this.Hide();
                if (admin.DialogResult == DialogResult.OK)
                {
                    this.Show();
                    Login.Text = "";
                    Password.Text = "";
                }
            }
            else if (position != "")
            {
                string sql1 = "SELECT EFirstname " +
                "FROM Employees " +
                "WHERE ELogin = '" + Login.Text + "' AND EPassword = '" + Password.Text + "'";

                string sql2 = "SELECT EName " +
                "FROM Employees " +
                "WHERE ELogin = '" + Login.Text + "' AND EPassword = '" + Password.Text + "'";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql1, connection);
                    firstName = com.ExecuteScalar().ToString();

                    SqlCommand com1 = new SqlCommand(sql2, connection);
                    name = com1.ExecuteScalar().ToString();

                }

                EmployeeForm employee;
                employee = new EmployeeForm();
                employee.EFirstName.Text = firstName;
                employee.EName.Text = name;
                employee.Position.Text = position;

                employee.ShowDialog();
                this.Hide();
                if (employee.DialogResult == DialogResult.OK)
                {
                    this.Show();
                    Login.Text = "";
                    Password.Text = "";
                }
            }
            else
                MessageBox.Show("Проверьте правильность введенных данных");
            
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
