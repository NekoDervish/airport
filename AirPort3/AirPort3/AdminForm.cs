﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AirPort3
{
    public partial class AdminForm : Form
    {
        public string id = "";

        public DataSet ds;
        public SqlDataAdapter adapter;
        public SqlCommandBuilder commandBuilder;
        public string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Airport.mdf;Integrated Security=True;Context Connection=False;TransparentNetworkIPResolution=False";
        public AdminForm()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            ACTable.Checked = true;
            string sql = "SELECT ACName AS 'Название авиакомпании' " +
                "FROM AirCompanies ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);

                ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
            }
            Filter1.Visible = false;
            Filter2.Visible = false;
            Filter3.Visible = false;
            Filter4.Visible = false;
            Filter5.Visible = false;
            Filter6.Visible = false;
        }

        private void ACTable_CheckedChanged(object sender, EventArgs e)
        {
            if (ACTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT ACName AS 'Название авиакомпании' " +
                "FROM AirCompanies ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
                Filter1.Visible = false;
                Filter2.Visible = false;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                id = "";
            }
        }

        private void APTable_CheckedChanged(object sender, EventArgs e)
        {
            if (APTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения' " +
                "FROM Airports ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По городу";
                Filter2.Text = "По названию аэропорта";

                id = "";
            }
        }

        private void BTable_CheckedChanged(object sender, EventArgs e)
        {
            if (BTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT BType AS 'Тип багажа' " +
                "FROM Baggage ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = false;
                Filter2.Visible = false;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                id = "";
            }
        }

        private void BPTable_CheckedChanged(object sender, EventArgs e)
        {
            if (BPTable.Checked)
            {
                AddButton.Enabled = false;
                UpdateButton.Enabled = false;
                string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета' " +
                "FROM BoardingPass";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру посадочного талона";
                Filter2.Text = "По номеру билета";

                id = "";
            }
        }

        private void ETable_CheckedChanged(object sender, EventArgs e)
        {
            if (ETable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = true;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру пасспорта";
                Filter2.Text = "По ФИО";
                Filter3.Text = "По полу";
                Filter4.Text = "По должности";

                id = "";
            }
        }

        private void FTable_CheckedChanged(object sender, EventArgs e)
        {
            if (FTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                "FROM Flights ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру рейса";
                Filter2.Text = "По городу отправления";
                Filter3.Text = "По городу прибытия";

                id = "";
            }
        }

        private void GTable_CheckedChanged(object sender, EventArgs e)
        {
            if (GTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT Gender AS 'Пол' " +
                "FROM Gender ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = false;
                Filter2.Visible = false;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                id = "";
            }
        }

        private void PTable_CheckedChanged(object sender, EventArgs e)
        {
            if (PTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = true;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Text = "По номеру паспорта";
                Filter2.Text = "По ФИО";
                Filter3.Text = "По полу";
                Filter4.Text = "По возрасту";

                Filter1.Checked = true;

                id = "";
            }
        }

        private void PLTable_CheckedChanged(object sender, EventArgs e)
        {
            if (PLTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                "FROM Planes ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Text = "По названию самолета"; 
                Filter1.Text = "По компании владельце";

                Filter1.Checked = true;

                id = "";
            }
        }

        private void PosTable_CheckedChanged(object sender, EventArgs e)
        {
            if (PosTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT Position AS 'Должность' " +
                "FROM Positions ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = false;
                Filter2.Visible = false;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                id = "";
            }
        }

        private void RTable_CheckedChanged(object sender, EventArgs e)
        {
            if (RTable.Checked)
            {
                AddButton.Enabled = false;
                UpdateButton.Enabled = false;
                string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                "FROM Registration";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру багажного талона";
                Filter2.Text = "По номеру билета";

                id = "";
            }
        }

        private void TTable_CheckedChanged(object sender, EventArgs e)
        {
            if (TTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = true;
                Filter5.Visible = true;
                Filter6.Visible = true;

                Filter1.Checked = true;

                Filter1.Text = "По номеру билета";
                Filter2.Text = "По номеру паспорта пасажира";
                Filter3.Text = "По номеру паспорта сотрудника";
                Filter4.Text = "По рейсу";
                Filter5.Text = "По дате отправления";

                id = "";
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddUpdForm add = new AddUpdForm();
            add.AddUpdText.Text = "Добавить информацию";

            Button OkButton = new Button();
            OkButton.Width = 137;
            OkButton.Height = 23;
            OkButton.Text = "Подтвердить";
            OkButton.DialogResult = DialogResult.OK;

            Button CancelButton = new Button();
            CancelButton.Width = 137;
            CancelButton.Height = 23;
            CancelButton.Text = "Отменить";
            CancelButton.DialogResult = DialogResult.Cancel;

            if (ACTable.Checked)
            {
                //Labels
                Label ACLabel1 = new Label();
                ACLabel1.Text = "Название авиакомпании";
                ACLabel1.Location = new Point(14, 63);
                ACLabel1.AutoSize = true;
                ACLabel1.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox ACText1 = new TextBox();
                ACText1.Location = new Point(17, 79);
                ACText1.Width = 220;
                ACText1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 116);
                CancelButton.Location = new Point(160, 116);

                //Form
                add.Width = 316;
                add.Height = 155;

                add.Controls.Add(ACLabel1);
                add.Controls.Add(ACText1);
                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();
                if (add.DialogResult == DialogResult.OK)
                {
                    if (ACText1.Text != "")
                    {
                        string sql1 = "INSERT INTO AirCompanies (ACName) VALUES (@ACName)";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@ACname", ACText1.Text);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поля не должны быть пустыми!\nКоманда не была выполнена!");

                    string sql = "SELECT ACName AS 'Название авиакомпании' " +
                "FROM AirCompanies ";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
            }

            if (APTable.Checked)
            {
                //Labels
                Label APLabel1 = new Label();
                APLabel1.Text = "Название города";
                APLabel1.Location = new Point(14, 63);
                APLabel1.AutoSize = true;
                APLabel1.BackColor = System.Drawing.Color.Transparent;

                Label APLabel2 = new Label();
                APLabel2.Text = "Название аэропорта";
                APLabel2.Location = new Point(14, 100);
                APLabel2.AutoSize = true;
                APLabel2.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox APText1 = new TextBox();
                APText1.Location = new Point(17, 79);
                APText1.Width = 220;
                APText1.Height = 20;

                TextBox APText2 = new TextBox();
                APText2.Location = new Point(17, 116);
                APText2.Width = 220;
                APText2.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 155);
                CancelButton.Location = new Point(160, 155);

                //Form
                add.Width = 316;
                add.Height = 195;

                add.Controls.Add(APLabel1);
                add.Controls.Add(APLabel2);

                add.Controls.Add(APText1);
                add.Controls.Add(APText2);

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();
                string APCity = "", APName = "";
                if (add.DialogResult == DialogResult.OK)
                {
                    APCity = APText1.Text;
                    APName = APText2.Text;

                    
                }
                if (APCity != "" && APName != "")
                {
                    string sql1 = "INSERT INTO AirPorts (ACity, AName) VALUES (@ACity, @AName)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql1;
                        cmd.Parameters.AddWithValue("@ACity", APCity);
                        cmd.Parameters.AddWithValue("@AName", APName);
                        cmd.ExecuteNonQuery();
                    }     
                }
                else
                    MessageBox.Show("Поля не должны быть пустыми!\nКоманда не была выполнена!");
                string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения' " +
                "FROM Airports ";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }

            if (BTable.Checked)
            {
                //Labels
                Label BLabel1 = new Label();
                BLabel1.Text = "Тип багажа";
                BLabel1.Location = new Point(14, 63);
                BLabel1.AutoSize = true;
                BLabel1.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox BText1 = new TextBox();
                BText1.Location = new Point(17, 79);
                BText1.Width = 220;
                BText1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 116);
                CancelButton.Location = new Point(160, 116);

                //Form
                add.Width = 316;
                add.Height = 155;
                add.Controls.Add(BLabel1);
                add.Controls.Add(BText1);

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                

                string BType = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    BType = BText1.Text;

                    

                }
                if(BType != "")
                {
                    string sql1 = "INSERT INTO Baggage (BType) VALUES (@BType)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql1;
                        cmd.Parameters.AddWithValue("@BType", BType);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    MessageBox.Show("Поле должно быть заполнено");   
                }
                string sql = "SELECT BType AS 'Тип багажа' " +
                "FROM Baggage ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            }

            if (ETable.Checked)
            {
                //Labels
                Label ELabel1 = new Label();
                ELabel1.Text = "№ паспорта";
                ELabel1.Location = new Point(14, 63);
                ELabel1.AutoSize = true;
                ELabel1.BackColor = System.Drawing.Color.Transparent;

                Label ELabel2 = new Label();
                ELabel2.Text = "Фамилия сотрудника";
                ELabel2.Location = new Point(14, 100);
                ELabel2.AutoSize = true;
                ELabel2.BackColor = System.Drawing.Color.Transparent;

                Label ELabel3 = new Label();
                ELabel3.Text = "Имя сотрудника";
                ELabel3.Location = new Point(14, 139);
                ELabel3.AutoSize = true;
                ELabel3.BackColor = System.Drawing.Color.Transparent;

                Label ELabel4 = new Label();
                ELabel4.Text = "Отчество сотрудника";
                ELabel4.Location = new Point(14, 179);
                ELabel4.AutoSize = true;
                ELabel4.BackColor = System.Drawing.Color.Transparent;

                Label ELabel5 = new Label();
                ELabel5.Text = "Пол";
                ELabel5.Location = new Point(252, 63);
                ELabel5.AutoSize = true;
                ELabel5.BackColor = System.Drawing.Color.Transparent;

                Label ELabel6 = new Label();
                ELabel6.Text = "Должность";
                ELabel6.Location = new Point(252, 100);
                ELabel6.AutoSize = true;
                ELabel6.BackColor = System.Drawing.Color.Transparent;

                Label ELabel7 = new Label();
                ELabel7.Text = "Логин";
                ELabel7.Location = new Point(252, 139);
                ELabel7.AutoSize = true;
                ELabel7.BackColor = System.Drawing.Color.Transparent;

                Label ELabel8 = new Label();
                ELabel8.Text = "Пароль";
                ELabel8.Location = new Point(252, 179);
                ELabel8.AutoSize = true;
                ELabel8.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox EText1 = new TextBox();
                EText1.Location = new Point(17, 79);
                EText1.Width = 220;
                EText1.Height = 20;
                EText1.MaxLength = 9;

                TextBox EText2 = new TextBox();
                EText2.Location = new Point(17, 116);
                EText2.Width = 220;
                EText2.Height = 20;

                TextBox EText3 = new TextBox();
                EText3.Location = new Point(17, 155);
                EText3.Width = 220;
                EText3.Height = 20;

                TextBox EText4 = new TextBox();
                EText4.Location = new Point(17, 195);
                EText4.Width = 220;
                EText4.Height = 20;

                TextBox EText5 = new TextBox();
                EText5.Location = new Point(255, 155);
                EText5.Width = 220;
                EText5.Height = 20;

                TextBox EText6 = new TextBox();
                EText6.Location = new Point(255, 195);
                EText6.Width = 220;
                EText6.Height = 20;

                //ComboBoxes
                ComboBox EBox1 = new ComboBox();
                EBox1.Location = new Point(255, 79);
                EBox1.Width = 220;
                EBox1.Height = 20;

                ComboBox EBox2 = new ComboBox();
                EBox2.Location = new Point(255, 116);
                EBox2.Width = 220;
                EBox2.Height = 20;

                //Buttons
                OkButton.Location = new Point(100, 237);
                CancelButton.Location = new Point(255, 237);

                //Form
                add.Width = 487;
                add.Height = 274;

                add.Controls.Add(ELabel1);
                add.Controls.Add(ELabel2);
                add.Controls.Add(ELabel3);
                add.Controls.Add(ELabel4);
                add.Controls.Add(ELabel5);
                add.Controls.Add(ELabel6);
                add.Controls.Add(ELabel7);
                add.Controls.Add(ELabel8);

                add.Controls.Add(EText1);
                add.Controls.Add(EText2);
                add.Controls.Add(EText3);
                add.Controls.Add(EText4);
                add.Controls.Add(EText5);
                add.Controls.Add(EText6);

                add.Controls.Add(EBox1);
                add.Controls.Add(EBox2);

                string sql1 = "SELECT * FROM Gender";
                string sql2 = "SELECT * FROM Positions";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    EBox1.DataSource = ds.Tables[0];
                    EBox1.DisplayMember = "Gender";
                    EBox1.ValueMember = "Gender";


                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql2, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    EBox2.DataSource = ds.Tables[0];
                    EBox2.DisplayMember = "Position";
                    EBox2.ValueMember = "Position";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string EPass = "";
                string EFname = "";
                string EName = "";
                string ESname = "";
                string EGender = "";
                string EPos = "";
                string ELogin = "";
                string EPassword = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    EPass = EText1.Text;
                    EFname = EText2.Text;
                    EName = EText3.Text;
                    ESname = EText4.Text;
                    ELogin = EText5.Text;
                    EPassword = EText6.Text;

                    EGender = EBox1.SelectedValue.ToString();
                    EPos = EBox2.SelectedValue.ToString();

                    
                }

                if (EPass != "" && EFname != "" && EName != "" && ESname != "" && EGender != "" && EPos != "" && ELogin != "" && EPassword != "")
                {
                    string sql3 = "INSERT INTO Employees (EPassport, EFirstname, EName, ESurname, EGender, EPos, ELogin, EPassword) VALUES (@EPass, @EFname, @EName, @ESname, @EGender, @EPos, @ELogin, @EPassword)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql3;
                        cmd.Parameters.AddWithValue("@EPass", EPass);
                        cmd.Parameters.AddWithValue("@EFname", EFname);
                        cmd.Parameters.AddWithValue("@EName", EName);
                        cmd.Parameters.AddWithValue("@ESname", ESname);
                        cmd.Parameters.AddWithValue("@EGender", EGender);
                        cmd.Parameters.AddWithValue("@EPos", EPos);
                        cmd.Parameters.AddWithValue("@ELogin", ELogin);
                        cmd.Parameters.AddWithValue("@EPassword", EPassword);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Заполните все поля");
                string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
               "FROM Employees ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            }

            if (FTable.Checked)
            {
                //Labels
                Label FLabel1 = new Label();
                FLabel1.Text = "Город отправления";
                FLabel1.Location = new Point(14, 63);
                FLabel1.AutoSize = true;
                FLabel1.BackColor = System.Drawing.Color.Transparent;

                Label FLabel2 = new Label();
                FLabel2.Text = "Город прибытия";
                FLabel2.Location = new Point(14, 100);
                FLabel2.AutoSize = true;
                FLabel2.BackColor = System.Drawing.Color.Transparent;

                Label FLabel3 = new Label();
                FLabel3.Text = "Продолжительность(часы)";
                FLabel3.Location = new Point(14, 139);
                FLabel3.AutoSize = true;
                FLabel3.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox FText1 = new TextBox();
                FText1.Location = new Point(17, 155);

                //ComboBoxes
                ComboBox FBox1 = new ComboBox();
                FBox1.Location = new Point(17, 79);
                FBox1.Width = 220;
                FBox1.Height = 20;

                ComboBox FBox2 = new ComboBox();
                FBox2.Location = new Point(17, 116);
                FBox2.Width = 220;
                FBox2.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 195);
                CancelButton.Location = new Point(160, 195);

                //Form
                add.Width = 316;
                add.Height = 237;

                add.Controls.Add(FLabel1);
                add.Controls.Add(FLabel2);
                add.Controls.Add(FLabel3);

                add.Controls.Add(FText1);

                add.Controls.Add(FBox1);
                add.Controls.Add(FBox2);

                string sql1 = "SELECT AName, CONCAT(ACity, '-',AName) As AP FROM Airports";
                string sql2 = "SELECT AName, CONCAT(ACity, '-',AName) As AP FROM Airports";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    FBox1.DataSource = ds.Tables[0];
                    FBox1.DisplayMember = "AP";
                    FBox1.ValueMember = "AName";


                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql2, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    FBox2.DataSource = ds.Tables[0];
                    FBox2.DisplayMember = "AP";
                    FBox2.ValueMember = "AName";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string FADepp = "";
                string FAArr = "";
                string FDur = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    FADepp = FBox1.SelectedValue.ToString();
                    FAArr = FBox2.SelectedValue.ToString();
                    FDur = FText1.Text;

                    
                }
                if (FADepp != "" && FAArr != "" && FDur != "")
                {
                    string sql3 = "INSERT INTO Flights (FADep, FAArr, FDuration) VALUES (@FADepp, @FAArr, @FDur)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql3;
                        cmd.Parameters.AddWithValue("@FADepp", FADepp);
                        cmd.Parameters.AddWithValue("@FAArr", FAArr);
                        cmd.Parameters.AddWithValue("@FDur", FDur);

                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Заполните все поля");
                string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                "FROM Flights ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }

            }

            if (GTable.Checked)
            {
                //Labels
                Label GLabel1 = new Label();
                GLabel1.Text = "Пол";
                GLabel1.Location = new Point(14, 63);
                GLabel1.AutoSize = true;
                GLabel1.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox GText1 = new TextBox();
                GText1.Location = new Point(17, 79);
                GText1.Width = 220;
                GText1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 116);
                CancelButton.Location = new Point(160, 116);

                //Form
                add.Width = 316;
                add.Height = 155;
                add.Controls.Add(GLabel1);
                add.Controls.Add(GText1);

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string Gender = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    Gender = GText1.Text;

                }

                if (Gender != "")
                {
                    string sql1 = "INSERT INTO Gender (Gender) VALUES (@Gender)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql1;
                        cmd.Parameters.AddWithValue("@Gender", Gender);

                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");

                string sql = "SELECT Gender AS 'Пол' " +
            "FROM Gender ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            }

            if (PTable.Checked)
            {
                //Labels
                Label PLabel1 = new Label();
                PLabel1.Text = "№ паспорта";
                PLabel1.Location = new Point(14, 63);
                PLabel1.AutoSize = true;
                PLabel1.BackColor = System.Drawing.Color.Transparent;

                Label PLabel2 = new Label();
                PLabel2.Text = "Фамилия пассажира";
                PLabel2.Location = new Point(14, 100);
                PLabel2.AutoSize = true;
                PLabel2.BackColor = System.Drawing.Color.Transparent;

                Label PLabel3 = new Label();
                PLabel3.Text = "Имя пассажира";
                PLabel3.Location = new Point(14, 139);
                PLabel3.AutoSize = true;
                PLabel3.BackColor = System.Drawing.Color.Transparent;

                Label PLabel4 = new Label();
                PLabel4.Text = "Отчество пассажира";
                PLabel4.Location = new Point(14, 179);
                PLabel4.AutoSize = true;
                PLabel4.BackColor = System.Drawing.Color.Transparent;

                Label PLabel5 = new Label();
                PLabel5.Text = "Пол";
                PLabel5.Location = new Point(252, 63);
                PLabel5.AutoSize = true;
                PLabel5.BackColor = System.Drawing.Color.Transparent;

                Label PLabel6 = new Label();
                PLabel6.Text = "Возраст";
                PLabel6.Location = new Point(252, 100);
                PLabel6.AutoSize = true;
                PLabel6.BackColor = System.Drawing.Color.Transparent;


                //TextBoxes
                TextBox PText1 = new TextBox();
                PText1.Location = new Point(17, 79);
                PText1.Width = 220;
                PText1.Height = 20;
                PText1.MaxLength = 9;

                TextBox PText2 = new TextBox();
                PText2.Location = new Point(17, 116);
                PText2.Width = 220;
                PText2.Height = 20;

                TextBox PText3 = new TextBox();
                PText3.Location = new Point(17, 155);
                PText3.Width = 220;
                PText3.Height = 20;

                TextBox PText4 = new TextBox();
                PText4.Location = new Point(17, 195);
                PText4.Width = 220;
                PText4.Height = 20;

                TextBox PText5 = new TextBox();
                PText5.Location = new Point(255, 116);
                PText5.Width = 220;
                PText5.Height = 20;

                //ComboBoxes
                ComboBox PBox1 = new ComboBox();
                PBox1.Location = new Point(255, 79);
                PBox1.Width = 220;
                PBox1.Height = 20;

                //Buttons
                OkButton.Location = new Point(100, 237);
                CancelButton.Location = new Point(255, 237);

                //Form
                add.Width = 487;
                add.Height = 274;

                add.Controls.Add(PLabel1);
                add.Controls.Add(PLabel2);
                add.Controls.Add(PLabel3);
                add.Controls.Add(PLabel4);
                add.Controls.Add(PLabel5);
                add.Controls.Add(PLabel6);

                add.Controls.Add(PText1);
                add.Controls.Add(PText2);
                add.Controls.Add(PText3);
                add.Controls.Add(PText4);
                add.Controls.Add(PText5);

                add.Controls.Add(PBox1);

                string sql1 = "SELECT * FROM Gender";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    PBox1.DataSource = ds.Tables[0];
                    PBox1.DisplayMember = "Gender";
                    PBox1.ValueMember = "Gender";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string PPass = "";
                string PFname = "";
                string PName = "";
                string PSname = "";
                string PGender = "";
                string PAge = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    PPass = PText1.Text;
                    PFname = PText2.Text;
                    PName = PText3.Text;
                    PSname = PText4.Text;
                    PAge = PText5.Text;
                    PGender = PBox1.SelectedValue.ToString();

                    
                }
                if (PPass != "" && PFname != "" && PName != "" && PSname != "" && PGender != "" && PAge != "")
                {
                    string sql2 = "INSERT INTO Passengers (PPassport, PFirstname, Pname, PSurname, PGender, PAge) VALUES (@PPass, @PFName, @PName, @PSname, @PGender, @PAge)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql2;
                        cmd.Parameters.AddWithValue("@PPass", PPass);
                        cmd.Parameters.AddWithValue("@PFname", PFname);
                        cmd.Parameters.AddWithValue("@PName", PName);
                        cmd.Parameters.AddWithValue("@PSname", PSname);
                        cmd.Parameters.AddWithValue("@PGender", PGender);
                        cmd.Parameters.AddWithValue("@PAge", PAge);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");
                string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }


            }

            if (PLTable.Checked)
            {
                //Labels
                Label PLLabel1 = new Label();
                PLLabel1.Text = "Название самолета";
                PLLabel1.Location = new Point(14, 63);
                PLLabel1.AutoSize = true;
                PLLabel1.BackColor = System.Drawing.Color.Transparent;

                Label PLLabel2 = new Label();
                PLLabel2.Text = "Авиакомпания";
                PLLabel2.Location = new Point(14, 100);
                PLLabel2.AutoSize = true;
                PLLabel2.BackColor = System.Drawing.Color.Transparent;

                Label PLLabel3 = new Label();
                PLLabel3.Text = "Количество мест";
                PLLabel3.Location = new Point(14, 139);
                PLLabel3.AutoSize = true;
                PLLabel3.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox PLText1 = new TextBox();
                PLText1.Location = new Point(17, 79);
                PLText1.Width = 220;
                PLText1.Height = 20;

                TextBox PLText2 = new TextBox();
                PLText2.Location = new Point(17, 155);
                PLText2.Width = 220;
                PLText2.Height = 20;

                //ComboBoxes
                ComboBox PLBox1 = new ComboBox();
                PLBox1.Location = new Point(17, 116);
                PLBox1.Width = 220;
                PLBox1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 195);
                CancelButton.Location = new Point(160, 195);

                //Form
                add.Width = 316;
                add.Height = 237;

                add.Controls.Add(PLLabel1);
                add.Controls.Add(PLLabel2);
                add.Controls.Add(PLLabel3);

                add.Controls.Add(PLText1);
                add.Controls.Add(PLText2);

                add.Controls.Add(PLBox1);

                string sql1 = "SELECT * FROM AirCompanies";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    PLBox1.DataSource = ds.Tables[0];
                    PLBox1.DisplayMember = "ACName";
                    PLBox1.ValueMember = "ACName";


                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string PLName = "";
                string PLCN = "";
                string PLNOS = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    PLName = PLText1.Text;
                    PLNOS = PLText2.Text;

                    PLCN = PLBox1.SelectedValue.ToString();

                    
                }

                if (PLName != "" && PLNOS != "" && PLCN != "")
                {
                    string sql2 = "INSERT INTO Planes (PLName, PLCompanyName, PLNumOfSeats) VALUES (@PLName, @PLCN, @PLNOS)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql2;
                        cmd.Parameters.AddWithValue("@PLName", PLName);
                        cmd.Parameters.AddWithValue("@PLNOS", PLNOS);
                        cmd.Parameters.AddWithValue("@PLCN", PLCN);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");
                string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                "FROM Planes ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            }

            if (PosTable.Checked)
            {
                //Labels
                Label PosLabel1 = new Label();
                PosLabel1.Text = "Название должности";
                PosLabel1.Location = new Point(14, 63);
                PosLabel1.AutoSize = true;
                PosLabel1.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox PosText1 = new TextBox();
                PosText1.Location = new Point(17, 79);
                PosText1.Width = 220;
                PosText1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 116);
                CancelButton.Location = new Point(160, 116);

                //Form
                add.Width = 316;
                add.Height = 155;
                add.Controls.Add(PosLabel1);
                add.Controls.Add(PosText1);

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string Position = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    Position = PosText1.Text;

                    
                }

                if (Position != "")
                {
                    string sql1 = "INSERT INTO Positions (Position) VALUES (@Position)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql1;
                        cmd.Parameters.AddWithValue("@Position", Position);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");
                string sql = "SELECT Position AS 'Должность' " +
                "FROM Positions ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            }

            if (TTable.Checked)
            {
                //Labels
                Label TLabel1 = new Label();
                TLabel1.Text = "Пассажир";
                TLabel1.Location = new Point(14, 63);
                TLabel1.AutoSize = true;
                TLabel1.BackColor = System.Drawing.Color.Transparent;

                Label TLabel2 = new Label();
                TLabel2.Text = "Сотрудник";
                TLabel2.Location = new Point(14, 100);
                TLabel2.AutoSize = true;
                TLabel2.BackColor = System.Drawing.Color.Transparent;

                Label TLabel3 = new Label();
                TLabel3.Text = "Рейс";
                TLabel3.Location = new Point(14, 139);
                TLabel3.AutoSize = true;
                TLabel3.BackColor = System.Drawing.Color.Transparent;

                Label TLabel4 = new Label();
                TLabel4.Text = "Самолет";
                TLabel4.Location = new Point(14, 179);
                TLabel4.AutoSize = true;
                TLabel4.BackColor = System.Drawing.Color.Transparent;

                Label TLabel5 = new Label();
                TLabel5.Text = "Дата и время отправления";
                TLabel5.Location = new Point(252, 63);
                TLabel5.AutoSize = true;
                TLabel5.BackColor = System.Drawing.Color.Transparent;

                Label TLabel6 = new Label();
                TLabel6.Text = "Место";
                TLabel6.Location = new Point(252, 100);
                TLabel6.AutoSize = true;
                TLabel6.BackColor = System.Drawing.Color.Transparent;

                Label TLabel7 = new Label();
                TLabel7.Text = "Багаж";
                TLabel7.Location = new Point(252, 139);
                TLabel7.AutoSize = true;
                TLabel7.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox TText1 = new TextBox();
                TText1.Location = new Point(255, 116);
                TText1.Width = 220;
                TText1.Height = 20;

                //ComboBoxes
                ComboBox TBox1 = new ComboBox();
                TBox1.Location = new Point(17, 79);
                TBox1.Width = 220;
                TBox1.Height = 20;

                ComboBox TBox2 = new ComboBox();
                TBox2.Location = new Point(17, 116);
                TBox2.Width = 220;
                TBox2.Height = 20;

                ComboBox TBox3 = new ComboBox();
                TBox3.Location = new Point(17, 155);
                TBox3.Width = 220;
                TBox3.Height = 20;

                ComboBox TBox4 = new ComboBox();
                TBox4.Location = new Point(17, 195);
                TBox4.Width = 220;
                TBox4.Height = 20;

                //CheckBoxes
                CheckBox TChBox1 = new CheckBox();
                TChBox1.Location = new Point(255, 155);

                //DateTimePicker
                DateTimePicker TDate1 = new DateTimePicker();
                TDate1.Location = new Point(255, 79);
                TDate1.Width = 220;
                TDate1.Height = 20;

                //Buttons
                OkButton.Location = new Point(100, 237);
                CancelButton.Location = new Point(255, 237);

                //Form
                add.Width = 487;
                add.Height = 274;

                add.Controls.Add(TLabel1);
                add.Controls.Add(TLabel2);
                add.Controls.Add(TLabel3);
                add.Controls.Add(TLabel4);
                add.Controls.Add(TLabel5);
                add.Controls.Add(TLabel6);
                add.Controls.Add(TLabel7);

                add.Controls.Add(TText1);

                add.Controls.Add(TBox1);
                add.Controls.Add(TBox2);
                add.Controls.Add(TBox3);
                add.Controls.Add(TBox4);

                add.Controls.Add(TChBox1);

                add.Controls.Add(TDate1);

                string sql1 = "SELECT PPassport, CONCAT(PFirstname, ' ', SUBSTRING(PName, 1, 1), '.',SUBSTRING(PSurname, 1, 1)) AS Pas FROM Passengers";
                string sql2 = "SELECT EPassport, CONCAT(EFirstname, ' ', SUBSTRING(EName, 1, 1), '.',SUBSTRING(ESurname, 1, 1)) AS Emp FROM Employees";
                string sql3 = "SELECT FId, CONCAT(FADep, '-', FAArr) AS Fl FROM Flights";
                string sql4 = "SELECT * FROM Planes";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox1.DataSource = ds.Tables[0];
                    TBox1.DisplayMember = "Pas";
                    TBox1.ValueMember = "PPassport";


                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql2, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox2.DataSource = ds.Tables[0];
                    TBox2.DisplayMember = "Emp";
                    TBox2.ValueMember = "EPassport";
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql3, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox3.DataSource = ds.Tables[0];
                    TBox3.DisplayMember = "Fl";
                    TBox3.ValueMember = "FId";
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql4, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox4.DataSource = ds.Tables[0];
                    TBox4.DisplayMember = "PLName";
                    TBox4.ValueMember = "PLName";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string TPass = "";
                string TEmpl = "";
                string TFlight = "";
                DateTime TDateTime = TDate1.Value;
                string TPlane = "";
                string TSeat = "";
                bool TBaggage = false;

                if (add.DialogResult == DialogResult.OK)
                {
                    TPass = TBox1.SelectedValue.ToString();
                    TEmpl = TBox2.SelectedValue.ToString();
                    TFlight = TBox3.SelectedValue.ToString();
                    TPlane = TBox4.SelectedValue.ToString();
                    TDateTime = TDate1.Value;
                    TSeat = TText1.Text;
                    TBaggage = TChBox1.Checked;

                    
                }

                if (TPass != "")
                {
                    string sql8 = "INSERT INTO Tickets (TPass, TEmpl, TFlight, TDateTime, TPlane, TSeat, TBaggage) VALUES (@TPass, @TEmpl, @TFlight, @TDateTime, @TPlane, @TSeat, @TBaggage)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql8;
                        cmd.Parameters.AddWithValue("@TPass", TPass);
                        cmd.Parameters.AddWithValue("@TEmpl", TEmpl);
                        cmd.Parameters.AddWithValue("@TFlight", TFlight);
                        cmd.Parameters.AddWithValue("@TPlane", TPlane);
                        cmd.Parameters.AddWithValue("@TDateTime", TDateTime);
                        cmd.Parameters.AddWithValue("@TSeat", TSeat);
                        cmd.Parameters.AddWithValue("@TBaggage", TBaggage);
                        cmd.ExecuteNonQuery();
                    }
                    if (TBaggage)
                    {
                        BaggageForm bf = new BaggageForm();

                        string sql5 = "SELECT * FROM Baggage";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql5, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            bf.comboBox1.DataSource = ds.Tables[0];
                            bf.comboBox1.DisplayMember = "BType";
                            bf.comboBox1.ValueMember = "BType";
                        }

                        string sql6 = "SELECT MAX(TicketId) FROM TICKETS";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            SqlCommand com = new SqlCommand(sql6, connection);
                            bf.textBox2.Text = com.ExecuteScalar().ToString();
                        }

                        bf.ShowDialog();
                        if(bf.DialogResult == DialogResult.OK)
                        {
                            string sql7 = "INSERT INTO Registration (RTicket, RBType, RBWeight) VALUES (@RTicket, @RBType, @RBWeight)";
                            using (SqlConnection connection = new SqlConnection(connectionString))

                            {
                                connection.Open();
                                SqlCommand cmd = new SqlCommand();
                                cmd.Connection = connection;
                                cmd.CommandText = sql7;
                                cmd.Parameters.AddWithValue("@RTicket", bf.textBox2.Text);
                                cmd.Parameters.AddWithValue("@RBType", bf.comboBox1.SelectedValue);
                                cmd.Parameters.AddWithValue("@RBWeight", bf.textBox4.Text);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");
                
            }

        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            
        }

        private void Search_TextChanged(object sender, EventArgs e)
        {
            if (ACTable.Checked)
            {
                string sql = "SELECT ACName AS 'Название авиакомпании' " +
                "FROM AirCompanies " +
                "WHERE ACName LIKE CONCAT(@Search, '%')";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql, connection);
                    com.Parameters.AddWithValue("@Search", Search.Text);
                    adapter = new SqlDataAdapter(com);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }

            if (APTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения'" +
                        "FROM AirPorts " +
                        "WHERE ACity LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                
               
                if (Filter2.Checked)
                {
                    string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения' " +
                        "FROM AirPorts " +
                        "WHERE AName LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                }
            

            if (BTable.Checked)
            {
                string sql = "SELECT BType AS 'Тип багажа' " +
                    "FROM Baggage " +
                    "WHERE BType LIKE CONCAT(@Search, '%')";
                using (SqlConnection connection = new SqlConnection(connectionString))

                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql, connection);
                    com.Parameters.AddWithValue("@Search", Search.Text);
                    adapter = new SqlDataAdapter(com);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                } 
            }

            if (BPTable.Checked)
            {
                if(Filter1.Checked)
                {
                    string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета' " +
                    "FROM BoardingPass " +
                    "WHERE BPId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета' " +
                    "FROM BoardingPass " +
                    "WHERE BPTId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                
            }

            if (ETable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees " +
                "WHERE EPassport LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees " +
                "WHERE EFirstname LIKE CONCAT(@Search, '%') OR EName LIKE CONCAT('%' ,@Search, '%') OR ESurname LIKE CONCAT('%', @Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter3.Checked)
                {
                    string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees " +
                "WHERE EGender LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter4.Checked)
                {
                    string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees " +
                "WHERE EPos LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                

            }

            if (FTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                    "FROM Flights " +
                    "WHERE FId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                    "FROM Flights " +
                    "WHERE FADep LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter3.Checked)
                {
                    string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                    "FROM Flights " +
                    "WHERE FAArr LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
            }

            if (GTable.Checked)
            {
                string sql = "SELECT Gender AS 'Пол' " +
                "FROM Gender " +
                "WHERE Gender LIKE CONCAT(@Search, '%')";
                using (SqlConnection connection = new SqlConnection(connectionString))

                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql, connection);
                    com.Parameters.AddWithValue("@Search", Search.Text);
                    adapter = new SqlDataAdapter(com);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }

            if (PTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PPassport LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PFirstname LIKE CONCAT(@Search, '%') OR PName LIKE CONCAT('%', @Search, '%') OR PSurname LIKE CONCAT('%', @Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com); adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter3.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PGender LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter4.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PAge LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                
            }

            if (PLTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                "FROM Planes " +
                "WHERE PLName LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                "FROM Planes " +
                "WHERE PLCompanyName LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
            }

            if (PosTable.Checked)
            {
                string sql = "SELECT Position AS 'Должность' " +
                "FROM Positions " +
                "WHERE Position LIKE CONCAT(@Search, '%')";
                using (SqlConnection connection = new SqlConnection(connectionString))

                {
                    connection.Open();
                    SqlCommand com = new SqlCommand(sql, connection);
                    com.Parameters.AddWithValue("@Search", Search.Text);
                    adapter = new SqlDataAdapter(com);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }

            if (RTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                    "FROM Registration " +
                    "WHERE RId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter2.Checked)
                {
                    string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                    "FROM Registration " +
                    "WHERE RTicket LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }

            if (TTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
"FROM Tickets " +
"WHERE TicketId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter2.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
"FROM Tickets " +
"WHERE TPass LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter3.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
"FROM Tickets " +
"WHERE TEmpl LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter4.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
"FROM Tickets " +
"WHERE TFlight LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter5.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
"FROM Tickets " +
"WHERE TDateTime LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Выберите объект таблицы");
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (id != "")
            {
                try
                {
                    if (ACTable.Checked)
                    {
                        string sql1 = "DELETE FROM AirCompanies WHERE ACName=@Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand(sql1, connection);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                          
                        }
                        string sql = "SELECT ACName AS 'Название авиакомпании' " +
             "FROM AirCompanies ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];
                        }
                    }

                    if (APTable.Checked)
                    {
                        string sql1 = "DELETE FROM AirPorts WHERE ACity = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения' " +
                "FROM Airports ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (BTable.Checked)
                    {
                        string sql1 = "DELETE FROM Baggage WHERE BType = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT BType AS 'Тип багажа' " +
               "FROM Baggage ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if(BPTable.Checked)
                    {
                        string sql1 = "DELETE FROM BoardingPass WHERE BPId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета' " +
               "FROM BoardingPass";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (ETable.Checked)
                    {
                        string sql1 = "DELETE FROM Employees WHERE EPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
                "FROM Employees ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (FTable.Checked)
                    {
                        string sql1 = "DELETE FROM Flights WHERE FId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                "FROM Flights ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (GTable.Checked)
                    {
                        string sql1 = "DELETE FROM Gender WHERE Gender = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT Gender AS 'Пол' " +
                "FROM Gender ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (PTable.Checked)
                    {
                        string sql1 = "DELETE FROM Passengers WHERE PPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;

                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (PLTable.Checked)
                    {
                        string sql1 = "DELETE FROM Planes WHERE PLName = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                "FROM Planes ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (PosTable.Checked)
                    {
                        string sql1 = "DELETE FROM Positions WHERE Position = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT Position AS 'Должность' " +
                "FROM Positions ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if(RTable.Checked)
                    {
                        string sql1 = "DELETE FROM Registration WHERE RId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                "FROM Registration";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (TTable.Checked)
                    {
                        string sql1 = "DELETE FROM Tickets WHERE TicketId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Невозможно удалить информацию! Проверьте доступность данных!");
                }
            }
            else
                MessageBox.Show("Выберите элемент базы данных");
        }

        private void UpdateButton_Click_1(object sender, EventArgs e)
        {
            if (id != "")
            {
                AddUpdForm add = new AddUpdForm();
                add.AddUpdText.Text = "Обновить информацию";

                Button OkButton = new Button();
                OkButton.Width = 137;
                OkButton.Height = 23;
                OkButton.Text = "Подтвердить";
                OkButton.DialogResult = DialogResult.OK;

                Button CancelButton = new Button();
                CancelButton.Width = 137;
                CancelButton.Height = 23;
                CancelButton.Text = "Отменить";
                CancelButton.DialogResult = DialogResult.Cancel;

                if (ACTable.Checked)
                {
                    //Labels
                    Label ACLabel1 = new Label();
                    ACLabel1.Text = "Название авиакомпании";
                    ACLabel1.Location = new Point(14, 63);
                    ACLabel1.AutoSize = true;
                    ACLabel1.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox ACText1 = new TextBox();
                    ACText1.Location = new Point(17, 79);
                    ACText1.Width = 220;
                    ACText1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 116);
                    CancelButton.Location = new Point(160, 116);

                    //Form
                    add.Width = 316;
                    add.Height = 155;

                    add.Controls.Add(ACLabel1);
                    add.Controls.Add(ACText1);
                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    ACText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                    add.ShowDialog();

                    if (add.DialogResult == DialogResult.OK)
                    {
                        if (ACText1.Text != "")
                        {
                            string sql1 = "UPDATE AirCompanies SET ACName = @ACName WHERE ACName = @Id";
                            using (SqlConnection connection = new SqlConnection(connectionString))

                            {
                                connection.Open();
                                SqlCommand cmd = new SqlCommand();
                                cmd.Connection = connection;
                                cmd.CommandText = sql1;
                                cmd.Parameters.AddWithValue("@ACname", ACText1.Text);
                                cmd.Parameters.AddWithValue("@Id", id);
                                cmd.ExecuteNonQuery();
                            }
                        }
                        else
                            MessageBox.Show("Поля не должны быть пустыми!\nКоманда не была выполнена!");

                        string sql = "SELECT ACName AS 'Название авиакомпании' " +
                    "FROM AirCompanies ";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];
                        }
                    }
                }

                if (APTable.Checked)
                {
                    //Labels
                    Label APLabel1 = new Label();
                    APLabel1.Text = "Название города";
                    APLabel1.Location = new Point(14, 63);
                    APLabel1.AutoSize = true;
                    APLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label APLabel2 = new Label();
                    APLabel2.Text = "Название аэропорта";
                    APLabel2.Location = new Point(14, 100);
                    APLabel2.AutoSize = true;
                    APLabel2.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox APText1 = new TextBox();
                    APText1.Location = new Point(17, 79);
                    APText1.Width = 220;
                    APText1.Height = 20;

                    TextBox APText2 = new TextBox();
                    APText2.Location = new Point(17, 116);
                    APText2.Width = 220;
                    APText2.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 155);
                    CancelButton.Location = new Point(160, 155);

                    //Form
                    add.Width = 316;
                    add.Height = 195;

                    add.Controls.Add(APLabel1);
                    add.Controls.Add(APLabel2);

                    add.Controls.Add(APText1);
                    add.Controls.Add(APText2);

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    APText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    APText2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();

                    add.ShowDialog();

                    string APCity = "", APName = "";
                    if (add.DialogResult == DialogResult.OK)
                    {
                        APCity = APText1.Text;
                        APName = APText2.Text;

                        string sql = "SELECT AName AS 'Название аэропорта', ACity AS 'Город разамещения' " +
                    "FROM Airports ";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];
                        }
                    }
                    if (APCity != "" && APName != "")
                    {
                        string sql1 = "UPDATE AirPorts SET ACity = @ACity, AName = @AName WHERE ACity = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@ACity", APCity);
                            cmd.Parameters.AddWithValue("@AName", APName);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поля не должны быть пустыми!\nКоманда не была выполнена!");
                }

                if (BTable.Checked)
                {
                    //Labels
                    Label BLabel1 = new Label();
                    BLabel1.Text = "Тип багажа";
                    BLabel1.Location = new Point(14, 63);
                    BLabel1.AutoSize = true;
                    BLabel1.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox BText1 = new TextBox();
                    BText1.Location = new Point(17, 79);
                    BText1.Width = 220;
                    BText1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 116);
                    CancelButton.Location = new Point(160, 116);

                    //Form
                    add.Width = 316;
                    add.Height = 155;
                    add.Controls.Add(BLabel1);
                    add.Controls.Add(BText1);

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    BText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                    add.ShowDialog();

                    string BType = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        BType = BText1.Text;

                        string sql = "SELECT BType AS 'Тип багажа' " +
                    "FROM Baggage ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
                    if (BType != "")
                    {
                        string sql1 = "UPDATE Baggage SET BType = @BType WHERE BType = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@BType", BType);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Поле должно быть заполнено");
                    }
                }

                if (ETable.Checked)
                {
                    //Labels
                    Label ELabel1 = new Label();
                    ELabel1.Text = "№ паспорта";
                    ELabel1.Location = new Point(14, 63);
                    ELabel1.AutoSize = true;
                    ELabel1.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel2 = new Label();
                    ELabel2.Text = "Фамилия сотрудника";
                    ELabel2.Location = new Point(14, 100);
                    ELabel2.AutoSize = true;
                    ELabel2.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel3 = new Label();
                    ELabel3.Text = "Имя сотрудника";
                    ELabel3.Location = new Point(14, 139);
                    ELabel3.AutoSize = true;
                    ELabel3.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel4 = new Label();
                    ELabel4.Text = "Отчество сотрудника";
                    ELabel4.Location = new Point(14, 179);
                    ELabel4.AutoSize = true;
                    ELabel4.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel5 = new Label();
                    ELabel5.Text = "Пол";
                    ELabel5.Location = new Point(252, 63);
                    ELabel5.AutoSize = true;
                    ELabel5.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel6 = new Label();
                    ELabel6.Text = "Должность";
                    ELabel6.Location = new Point(252, 100);
                    ELabel6.AutoSize = true;
                    ELabel6.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel7 = new Label();
                    ELabel7.Text = "Логин";
                    ELabel7.Location = new Point(252, 139);
                    ELabel7.AutoSize = true;
                    ELabel7.BackColor = System.Drawing.Color.Transparent;

                    Label ELabel8 = new Label();
                    ELabel8.Text = "Пароль";
                    ELabel8.Location = new Point(252, 179);
                    ELabel8.AutoSize = true;
                    ELabel8.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox EText1 = new TextBox();
                    EText1.Location = new Point(17, 79);
                    EText1.Width = 220;
                    EText1.Height = 20;
                    EText1.MaxLength = 9;

                    TextBox EText2 = new TextBox();
                    EText2.Location = new Point(17, 116);
                    EText2.Width = 220;
                    EText2.Height = 20;

                    TextBox EText3 = new TextBox();
                    EText3.Location = new Point(17, 155);
                    EText3.Width = 220;
                    EText3.Height = 20;

                    TextBox EText4 = new TextBox();
                    EText4.Location = new Point(17, 195);
                    EText4.Width = 220;
                    EText4.Height = 20;

                    TextBox EText5 = new TextBox();
                    EText5.Location = new Point(255, 155);
                    EText5.Width = 220;
                    EText5.Height = 20;

                    TextBox EText6 = new TextBox();
                    EText6.Location = new Point(255, 195);
                    EText6.Width = 220;
                    EText6.Height = 20;

                    //ComboBoxes
                    ComboBox EBox1 = new ComboBox();
                    EBox1.Location = new Point(255, 79);
                    EBox1.Width = 220;
                    EBox1.Height = 20;

                    ComboBox EBox2 = new ComboBox();
                    EBox2.Location = new Point(255, 116);
                    EBox2.Width = 220;
                    EBox2.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(100, 237);
                    CancelButton.Location = new Point(255, 237);

                    //Form
                    add.Width = 487;
                    add.Height = 274;

                    add.Controls.Add(ELabel1);
                    add.Controls.Add(ELabel2);
                    add.Controls.Add(ELabel3);
                    add.Controls.Add(ELabel4);
                    add.Controls.Add(ELabel5);
                    add.Controls.Add(ELabel6);
                    add.Controls.Add(ELabel7);
                    add.Controls.Add(ELabel8);

                    add.Controls.Add(EText1);
                    add.Controls.Add(EText2);
                    add.Controls.Add(EText3);
                    add.Controls.Add(EText4);
                    add.Controls.Add(EText5);
                    add.Controls.Add(EText6);

                    add.Controls.Add(EBox1);
                    add.Controls.Add(EBox2);

                    string sql1 = "SELECT * FROM Gender";
                    string sql2 = "SELECT * FROM Positions";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        EBox1.DataSource = ds.Tables[0];
                        EBox1.DisplayMember = "Gender";
                        EBox1.ValueMember = "Gender";


                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql2, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        EBox2.DataSource = ds.Tables[0];
                        EBox2.DisplayMember = "Position";
                        EBox2.ValueMember = "Position";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    EText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    EText2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    EText3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    EText4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    EText5.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
                    EText6.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();

                    EBox1.SelectedValue = dataGridView1.CurrentRow.Cells[4].Value;
                    EBox2.SelectedValue = dataGridView1.CurrentRow.Cells[5].Value;


                    add.ShowDialog();

                    string EPass = "";
                    string EFname = "";
                    string EName = "";
                    string ESname = "";
                    string EGender = "";
                    string EPos = "";
                    string ELogin = "";
                    string EPassword = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        EPass = EText1.Text;
                        EFname = EText2.Text;
                        EName = EText3.Text;
                        ESname = EText4.Text;
                        ELogin = EText5.Text;
                        EPassword = EText6.Text;

                        EGender = EBox1.SelectedValue.ToString();
                        EPos = EBox2.SelectedValue.ToString();


                    }

                    if (EPass != "" && EFname != "" && EName != "" && ESname != "" && EGender != "" && EPos != "" && ELogin != "" && EPassword != "")
                    {
                        string sql3 = "UPDATE Employees SET EPassport = @EPass, EFirstname = @EFname, EName = @EName, ESurname = @ESname, EGender = @EGender, EPos = @EPos, ELogin = @ELogin, EPassword = @EPassword WHERE EPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql3;
                            cmd.Parameters.AddWithValue("@EPass", EPass);
                            cmd.Parameters.AddWithValue("@EFname", EFname);
                            cmd.Parameters.AddWithValue("@EName", EName);
                            cmd.Parameters.AddWithValue("@ESname", ESname);
                            cmd.Parameters.AddWithValue("@EGender", EGender);
                            cmd.Parameters.AddWithValue("@EPos", EPos);
                            cmd.Parameters.AddWithValue("@ELogin", ELogin);
                            cmd.Parameters.AddWithValue("@EPassword", EPassword);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Заполните все поля");
                    string sql = "SELECT EPassport AS 'Номер паспорта', EFirstname AS 'Фамилия', EName AS 'Имя', ESurname AS 'Отчество', EGender AS 'Пол', EPos AS 'Должность', ELogin AS 'Логин', EPassword AS 'Пароль' " +
               "FROM Employees ";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }

                }

                if (FTable.Checked)
                {
                    //Labels
                    Label FLabel1 = new Label();
                    FLabel1.Text = "Город отправления";
                    FLabel1.Location = new Point(14, 63);
                    FLabel1.AutoSize = true;
                    FLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label FLabel2 = new Label();
                    FLabel2.Text = "Город прибытия";
                    FLabel2.Location = new Point(14, 100);
                    FLabel2.AutoSize = true;
                    FLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label FLabel3 = new Label();
                    FLabel3.Text = "Продолжительность(часы)";
                    FLabel3.Location = new Point(14, 139);
                    FLabel3.AutoSize = true;
                    FLabel3.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox FText1 = new TextBox();
                    FText1.Location = new Point(17, 155);

                    //ComboBoxes
                    ComboBox FBox1 = new ComboBox();
                    FBox1.Location = new Point(17, 79);
                    FBox1.Width = 220;
                    FBox1.Height = 20;

                    ComboBox FBox2 = new ComboBox();
                    FBox2.Location = new Point(17, 116);
                    FBox2.Width = 220;
                    FBox2.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 195);
                    CancelButton.Location = new Point(160, 195);

                    //Form
                    add.Width = 316;
                    add.Height = 237;

                    add.Controls.Add(FLabel1);
                    add.Controls.Add(FLabel2);
                    add.Controls.Add(FLabel3);

                    add.Controls.Add(FText1);

                    add.Controls.Add(FBox1);
                    add.Controls.Add(FBox2);

                    string sql1 = "SELECT AName, CONCAT(ACity, '-',AName) As AP FROM Airports";
                    string sql2 = "SELECT AName, CONCAT(ACity, '-',AName) As AP FROM Airports";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        FBox1.DataSource = ds.Tables[0];
                        FBox1.DisplayMember = "AP";
                        FBox1.ValueMember = "AName";


                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql2, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        FBox2.DataSource = ds.Tables[0];
                        FBox2.DisplayMember = "AP";
                        FBox2.ValueMember = "AName";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    FBox1.SelectedValue = dataGridView1.CurrentRow.Cells[1].Value;
                    FBox2.SelectedValue = dataGridView1.CurrentRow.Cells[2].Value;

                    FText1.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();

                    add.ShowDialog();

                    string FADepp = "";
                    string FAArr = "";
                    string FDur = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        FADepp = FBox1.ValueMember;
                        FAArr = FBox2.ValueMember;
                        FDur = FText1.Text;

                        string sql = "SELECT FId AS 'Номер рейса', FADep AS 'Город отправления', FAArr AS 'Город прибытия', FDuration AS 'Время полета, ч' " +
                    "FROM Flights ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
                    if (FADepp != "" && FAArr != "" && FDur != "")
                    {
                        string sql = "UPDATE Flights SET FADep = @FADepp, FAArr = @FAArr, FDuration = @FDur WHERE FId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@FADepp", FADepp);
                            cmd.Parameters.AddWithValue("@FAArr", FAArr);
                            cmd.Parameters.AddWithValue("@FDur", FDur);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Заполните все поля");

                }

                if (GTable.Checked)
                {
                    //Labels
                    Label GLabel1 = new Label();
                    GLabel1.Text = "Пол";
                    GLabel1.Location = new Point(14, 63);
                    GLabel1.AutoSize = true;
                    GLabel1.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox GText1 = new TextBox();
                    GText1.Location = new Point(17, 79);
                    GText1.Width = 220;
                    GText1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 116);
                    CancelButton.Location = new Point(160, 116);

                    //Form
                    add.Width = 316;
                    add.Height = 155;
                    add.Controls.Add(GLabel1);
                    add.Controls.Add(GText1);

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    GText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                    add.ShowDialog();

                    string Gender = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        Gender = GText1.Text;

                        string sql = "SELECT Gender AS 'Пол' " +
                    "FROM Gender ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (Gender != "")
                    {
                        string sql = "UPDATE Gender SET Gender = @Gender WHERE Gender = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@Gender", Gender);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");
                }

                if (PTable.Checked)
                {
                    //Labels
                    Label PLabel1 = new Label();
                    PLabel1.Text = "№ паспорта";
                    PLabel1.Location = new Point(14, 63);
                    PLabel1.AutoSize = true;
                    PLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel2 = new Label();
                    PLabel2.Text = "Фамилия пассажира";
                    PLabel2.Location = new Point(14, 100);
                    PLabel2.AutoSize = true;
                    PLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel3 = new Label();
                    PLabel3.Text = "Имя пассажира";
                    PLabel3.Location = new Point(14, 139);
                    PLabel3.AutoSize = true;
                    PLabel3.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel4 = new Label();
                    PLabel4.Text = "Отчество пассажира";
                    PLabel4.Location = new Point(14, 179);
                    PLabel4.AutoSize = true;
                    PLabel4.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel5 = new Label();
                    PLabel5.Text = "Пол";
                    PLabel5.Location = new Point(252, 63);
                    PLabel5.AutoSize = true;
                    PLabel5.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel6 = new Label();
                    PLabel6.Text = "Возраст";
                    PLabel6.Location = new Point(252, 100);
                    PLabel6.AutoSize = true;
                    PLabel6.BackColor = System.Drawing.Color.Transparent;


                    //TextBoxes
                    TextBox PText1 = new TextBox();
                    PText1.Location = new Point(17, 79);
                    PText1.Width = 220;
                    PText1.Height = 20;
                    PText1.MaxLength = 9;

                    TextBox PText2 = new TextBox();
                    PText2.Location = new Point(17, 116);
                    PText2.Width = 220;
                    PText2.Height = 20;

                    TextBox PText3 = new TextBox();
                    PText3.Location = new Point(17, 155);
                    PText3.Width = 220;
                    PText3.Height = 20;

                    TextBox PText4 = new TextBox();
                    PText4.Location = new Point(17, 195);
                    PText4.Width = 220;
                    PText4.Height = 20;

                    TextBox PText5 = new TextBox();
                    PText5.Location = new Point(255, 116);
                    PText5.Width = 220;
                    PText5.Height = 20;

                    //ComboBoxes
                    ComboBox PBox1 = new ComboBox();
                    PBox1.Location = new Point(255, 79);
                    PBox1.Width = 220;
                    PBox1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(100, 237);
                    CancelButton.Location = new Point(255, 237);

                    //Form
                    add.Width = 487;
                    add.Height = 274;

                    add.Controls.Add(PLabel1);
                    add.Controls.Add(PLabel2);
                    add.Controls.Add(PLabel3);
                    add.Controls.Add(PLabel4);
                    add.Controls.Add(PLabel5);
                    add.Controls.Add(PLabel6);

                    add.Controls.Add(PText1);
                    add.Controls.Add(PText2);
                    add.Controls.Add(PText3);
                    add.Controls.Add(PText4);
                    add.Controls.Add(PText5);

                    add.Controls.Add(PBox1);

                    string sql1 = "SELECT * FROM Gender";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        PBox1.DataSource = ds.Tables[0];
                        PBox1.DisplayMember = "Gender";
                        PBox1.ValueMember = "Gender";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    PText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    PText2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    PText3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    PText4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    PText5.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();

                    PBox1.SelectedValue = dataGridView1.CurrentRow.Cells[4].Value;

                    add.ShowDialog();

                    string PPass = "";
                    string PFname = "";
                    string PName = "";
                    string PSname = "";
                    string PGender = "";
                    string PAge = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        PPass = PText1.Text;
                        PFname = PText2.Text;
                        PName = PText3.Text;
                        PSname = PText4.Text;
                        PAge = PText5.Text;
                        PGender = PBox1.ValueMember;

                        string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                    "FROM Passengers ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
                    if (PPass != "" && PFname != "" && PName != "" && PSname != "" && PGender != "" && PAge != "")
                    {
                        string sql = "UPDATE Passengers SET PPassport = @PPass, PFirstname = @PFname, Pname = @PName, PSurname = @PSname, PGender = @PGender, PAge = @PAge WHERE PPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@PPass", PPass);
                            cmd.Parameters.AddWithValue("@PFname", PFname);
                            cmd.Parameters.AddWithValue("@PName", PName);
                            cmd.Parameters.AddWithValue("@PSname", PSname);
                            cmd.Parameters.AddWithValue("@PGender", PGender);
                            cmd.Parameters.AddWithValue("@PAge", PAge);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");


                }

                if (PLTable.Checked)
                {
                    //Labels
                    Label PLLabel1 = new Label();
                    PLLabel1.Text = "Название самолета";
                    PLLabel1.Location = new Point(14, 63);
                    PLLabel1.AutoSize = true;
                    PLLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label PLLabel2 = new Label();
                    PLLabel2.Text = "Авиакомпания";
                    PLLabel2.Location = new Point(14, 100);
                    PLLabel2.AutoSize = true;
                    PLLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label PLLabel3 = new Label();
                    PLLabel3.Text = "Количество мест";
                    PLLabel3.Location = new Point(14, 139);
                    PLLabel3.AutoSize = true;
                    PLLabel3.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox PLText1 = new TextBox();
                    PLText1.Location = new Point(17, 79);
                    PLText1.Width = 220;
                    PLText1.Height = 20;

                    TextBox PLText2 = new TextBox();
                    PLText2.Location = new Point(17, 155);
                    PLText2.Width = 220;
                    PLText2.Height = 20;

                    //ComboBoxes
                    ComboBox PLBox1 = new ComboBox();
                    PLBox1.Location = new Point(17, 116);
                    PLBox1.Width = 220;
                    PLBox1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 195);
                    CancelButton.Location = new Point(160, 195);

                    //Form
                    add.Width = 316;
                    add.Height = 237;

                    add.Controls.Add(PLLabel1);
                    add.Controls.Add(PLLabel2);
                    add.Controls.Add(PLLabel3);

                    add.Controls.Add(PLText1);
                    add.Controls.Add(PLText2);

                    add.Controls.Add(PLBox1);

                    string sql1 = "SELECT * FROM AirCompanies";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        PLBox1.DataSource = ds.Tables[0];
                        PLBox1.DisplayMember = "ACName";
                        PLBox1.ValueMember = "ACName";


                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    PLText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    PLText2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();

                    PLBox1.SelectedValue = dataGridView1.CurrentRow.Cells[1].Value;

                    add.ShowDialog();

                    string PLName = "";
                    string PLCN = "";
                    string PLNOS = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        PLName = PLText1.Text;
                        PLNOS = PLText2.Text;

                        PLCN = PLBox1.ValueMember;

                        string sql = "SELECT PLName AS 'Название самолета', PLCompanyName AS 'Компания владелец', PLNumOfSeats AS 'Количество мест' " +
                    "FROM Planes ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (PLName != "" && PLNOS != "" && PLCN != "")
                    {
                        string sql = "UPDATE Planes SET PLName = @PLName, PLCompanyName = @PLCN, PLNumOfSeats = @PLNOS WHERE PLName = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@PLName", PLName);
                            cmd.Parameters.AddWithValue("@PLNOS", PLNOS);
                            cmd.Parameters.AddWithValue("@PLCN", PLCN);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");
                }

                if (PosTable.Checked)
                {
                    //Labels
                    Label PosLabel1 = new Label();
                    PosLabel1.Text = "Название должности";
                    PosLabel1.Location = new Point(14, 63);
                    PosLabel1.AutoSize = true;
                    PosLabel1.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox PosText1 = new TextBox();
                    PosText1.Location = new Point(17, 79);
                    PosText1.Width = 220;
                    PosText1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(17, 116);
                    CancelButton.Location = new Point(160, 116);

                    //Form
                    add.Width = 316;
                    add.Height = 155;
                    add.Controls.Add(PosLabel1);
                    add.Controls.Add(PosText1);

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    PosText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                    add.ShowDialog();

                    string Position = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        Position = PosText1.Text;

                        string sql = "SELECT Position AS 'Должность' " +
                    "FROM Positions ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (Position != "")
                    {
                        string sql = "UPDATE Positions SET Position = @Position WHERE Position = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@Position", Position);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");
                }

                if (TTable.Checked)
                {
                    //Labels
                    Label TLabel1 = new Label();
                    TLabel1.Text = "Пассажир";
                    TLabel1.Location = new Point(14, 63);
                    TLabel1.AutoSize = true;
                    TLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel2 = new Label();
                    TLabel2.Text = "Сотрудник";
                    TLabel2.Location = new Point(14, 100);
                    TLabel2.AutoSize = true;
                    TLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel3 = new Label();
                    TLabel3.Text = "Рейс";
                    TLabel3.Location = new Point(14, 139);
                    TLabel3.AutoSize = true;
                    TLabel3.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel4 = new Label();
                    TLabel4.Text = "Самолет";
                    TLabel4.Location = new Point(14, 179);
                    TLabel4.AutoSize = true;
                    TLabel4.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel5 = new Label();
                    TLabel5.Text = "Дата и время отправления";
                    TLabel5.Location = new Point(252, 63);
                    TLabel5.AutoSize = true;
                    TLabel5.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel6 = new Label();
                    TLabel6.Text = "Место";
                    TLabel6.Location = new Point(252, 100);
                    TLabel6.AutoSize = true;
                    TLabel6.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel7 = new Label();
                    TLabel7.Text = "Багаж";
                    TLabel7.Location = new Point(252, 139);
                    TLabel7.AutoSize = true;
                    TLabel7.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox TText1 = new TextBox();
                    TText1.Location = new Point(255, 116);
                    TText1.Width = 220;
                    TText1.Height = 20;

                    //ComboBoxes
                    ComboBox TBox1 = new ComboBox();
                    TBox1.Location = new Point(17, 79);
                    TBox1.Width = 220;
                    TBox1.Height = 20;

                    ComboBox TBox2 = new ComboBox();
                    TBox2.Location = new Point(17, 116);
                    TBox2.Width = 220;
                    TBox2.Height = 20;

                    ComboBox TBox3 = new ComboBox();
                    TBox3.Location = new Point(17, 155);
                    TBox3.Width = 220;
                    TBox3.Height = 20;

                    ComboBox TBox4 = new ComboBox();
                    TBox4.Location = new Point(17, 195);
                    TBox4.Width = 220;
                    TBox4.Height = 20;

                    //CheckBoxes
                    CheckBox TChBox1 = new CheckBox();
                    TChBox1.Location = new Point(255, 155);

                    //DateTimePicker
                    DateTimePicker TDate1 = new DateTimePicker();
                    TDate1.Location = new Point(255, 79);
                    TDate1.Width = 220;
                    TDate1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(100, 237);
                    CancelButton.Location = new Point(255, 237);

                    //Form
                    add.Width = 487;
                    add.Height = 274;

                    add.Controls.Add(TLabel1);
                    add.Controls.Add(TLabel2);
                    add.Controls.Add(TLabel3);
                    add.Controls.Add(TLabel4);
                    add.Controls.Add(TLabel5);
                    add.Controls.Add(TLabel6);
                    add.Controls.Add(TLabel7);

                    add.Controls.Add(TText1);

                    add.Controls.Add(TBox1);
                    add.Controls.Add(TBox2);
                    add.Controls.Add(TBox3);
                    add.Controls.Add(TBox4);

                    add.Controls.Add(TChBox1);

                    add.Controls.Add(TDate1);

                    string sql1 = "SELECT PPassport, CONCAT(PFirstname, ' ', SUBSTRING(PName, 1, 1), '.',SUBSTRING(PSurname, 1, 1)) AS Pas FROM Passengers";
                    string sql2 = "SELECT EPassport, CONCAT(EFirstname, ' ', SUBSTRING(EName, 1, 1), '.',SUBSTRING(ESurname, 1, 1)) AS Emp FROM Employees";
                    string sql3 = "SELECT FId, CONCAT(FADep, '-', FAArr) AS Fl FROM Flights";
                    string sql4 = "SELECT * FROM Planes";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox1.DataSource = ds.Tables[0];
                        TBox1.DisplayMember = "Pas";
                        TBox1.ValueMember = "PPassport";


                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql2, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox2.DataSource = ds.Tables[0];
                        TBox2.DisplayMember = "Emp";
                        TBox2.ValueMember = "EPassport";
                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql3, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox3.DataSource = ds.Tables[0];
                        TBox3.DisplayMember = "Fl";
                        TBox3.ValueMember = "FId";
                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql4, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox4.DataSource = ds.Tables[0];
                        TBox4.DisplayMember = "PLName";
                        TBox4.ValueMember = "PLName";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    TBox1.SelectedValue = dataGridView1.CurrentRow.Cells[1].Value;
                    TBox2.SelectedValue = dataGridView1.CurrentRow.Cells[2].Value;
                    TBox3.SelectedValue = dataGridView1.CurrentRow.Cells[3].Value;
                    TBox4.SelectedValue = dataGridView1.CurrentRow.Cells[5].Value;

                    TDate1.Value = (DateTime)dataGridView1.CurrentRow.Cells[4].Value;

                    TText1.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();

                    TChBox1.Checked = (bool)dataGridView1.CurrentRow.Cells[7].Value;

                    add.ShowDialog();

                    string TPass = "";
                    string TEmpl = "";
                    string TFlight = "";
                    DateTime TDateTime = TDate1.Value;
                    string TPlane = "";
                    string TSeat = "";
                    bool TBaggage = false;

                    if (add.DialogResult == DialogResult.OK)
                    {
                        TPass = TBox1.ValueMember;
                        TEmpl = TBox2.ValueMember;
                        TFlight = TBox3.ValueMember;
                        TPlane = TBox4.ValueMember;
                        TDateTime = TDate1.Value;
                        TSeat = TText1.Text;
                        TBaggage = TChBox1.Checked;

                        string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', " +
                            "TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                    "FROM Tickets";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (TPass != "")
                    {
                        string sql = "UPDATE Tickets SET TPass = @TPass, TEmpl = @TEmpl, TFlight = @TFlight, TDateTime = @TDateTime, TPlane = @TPlane, TSeat = @TSeat, TBaggage = @TBaggage WHERE TicketID = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@TPass", TPass);
                            cmd.Parameters.AddWithValue("@TEmpl", TEmpl);
                            cmd.Parameters.AddWithValue("@TFlight", TFlight);
                            cmd.Parameters.AddWithValue("@TPlane", TPlane);
                            cmd.Parameters.AddWithValue("@TDateTime", TDateTime);
                            cmd.Parameters.AddWithValue("@TSeat", TSeat);
                            cmd.Parameters.AddWithValue("@TBaggage", TBaggage);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");

                }
            }
            else
                MessageBox.Show("Выберите элемент базы данных");
        }
    }
}
