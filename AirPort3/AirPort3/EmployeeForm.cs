﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AirPort3
{
    public partial class EmployeeForm : Form
    {
        public string id = "";

        public DataSet ds;
        public SqlDataAdapter adapter;
        public SqlCommandBuilder commandBuilder;
        public string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Airport.mdf;Integrated Security=True;Context Connection=False;TransparentNetworkIPResolution=False";
        public EmployeeForm()
        {
            InitializeComponent();
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {
            TTable.Checked = true;
            AddButton.Enabled = true;
            UpdateButton.Enabled = true;
            string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);

                ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];

            }
            Filter1.Visible = true;
            Filter2.Visible = true;
            Filter3.Visible = true;
            Filter4.Visible = true;
            Filter5.Visible = true;
            Filter6.Visible = true;

            Filter1.Checked = true;

            Filter1.Text = "По номеру билета";
            Filter2.Text = "По номеру паспорта пасажира";
            Filter3.Text = "По номеру паспорта сотрудника";
            Filter4.Text = "По рейсу";
            Filter5.Text = "По дате отправления";

            id = "";
        }

        private void TTable_CheckedChanged(object sender, EventArgs e)
        {
            if (TTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = true;
                Filter5.Visible = true;
                Filter6.Visible = true;

                Filter1.Checked = true;

                Filter1.Text = "По номеру билета";
                Filter2.Text = "По ФИО пасажира";
                Filter3.Text = "По ФИО сотрудника";
                Filter4.Text = "По рейсу";
                Filter5.Text = "По дате отправления";

                id = "";
            }
        }

        private void BPTable_CheckedChanged(object sender, EventArgs e)
        {
            if (BPTable.Checked)
            {
                AddButton.Enabled = false;
                UpdateButton.Enabled = false;
                string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(PName, 1, 1), SUBSTRING(PSurname, 1, 1)) AS 'ФИО пассажира', t.TSeat AS 'Место' " +
               "FROM BoardingPass bp " +
               "INNER JOIN Tickets t ON t.TicketId = bp.BPTId " +
               "INNER JOIN Passengers p ON p.PPassport = t.TPass";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру посадочного талона";
                Filter2.Text = "По номеру билета";

                id = "";
            }
        }

        private void PTable_CheckedChanged(object sender, EventArgs e)
        {
            if (PTable.Checked)
            {
                AddButton.Enabled = true;
                UpdateButton.Enabled = true;
                string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = true;
                Filter4.Visible = true;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Text = "По номеру паспорта";
                Filter2.Text = "По ФИО";
                Filter3.Text = "По полу";
                Filter4.Text = "По возрасту";

                Filter1.Checked = true;

                id = "";
            }
        }

        private void RTable_CheckedChanged(object sender, EventArgs e)
        {
            if (RTable.Checked)
            {
                AddButton.Enabled = false;
                UpdateButton.Enabled = false;
                string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                "FROM Registration";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }
                Filter1.Visible = true;
                Filter2.Visible = true;
                Filter3.Visible = false;
                Filter4.Visible = false;
                Filter5.Visible = false;
                Filter6.Visible = false;

                Filter1.Checked = true;

                Filter1.Text = "По номеру багажного талона";
                Filter2.Text = "По номеру билета";

                id = "";
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (id != "")
            {
                try
                {
                    if (BPTable.Checked)
                    {
                        string sql1 = "DELETE FROM BoardingPass WHERE BPId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета' " +
               "FROM BoardingPass";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
      
                    if (PTable.Checked)
                    {
                        string sql1 = "DELETE FROM Passengers WHERE PPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;

                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (RTable.Checked)
                    {
                        string sql1 = "DELETE FROM Registration WHERE RId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                "FROM Registration";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }

                    if (TTable.Checked)
                    {
                        string sql1 = "DELETE FROM Tickets WHERE TicketId = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                        string sql = "SELECT TicketId AS 'Номер билета', TPass AS 'Номер паспорта пассажира', TEmpl AS 'Номер паспорта сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets";

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            dataGridView1.DataSource = ds.Tables[0];

                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Невозможно удалить информацию! Проверьте доступность данных!");
                }
            }
            else
                MessageBox.Show("Выберите элемент базы данных");
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddUpdForm add = new AddUpdForm();
            add.AddUpdText.Text = "Добавить информацию";

            Button OkButton = new Button();
            OkButton.Width = 137;
            OkButton.Height = 23;
            OkButton.Text = "Подтвердить";
            OkButton.DialogResult = DialogResult.OK;

            Button CancelButton = new Button();
            CancelButton.Width = 137;
            CancelButton.Height = 23;
            CancelButton.Text = "Отменить";
            CancelButton.DialogResult = DialogResult.Cancel;

            if (PTable.Checked)
            {
                //Labels
                Label PLabel1 = new Label();
                PLabel1.Text = "№ паспорта";
                PLabel1.Location = new Point(14, 63);
                PLabel1.AutoSize = true;
                PLabel1.BackColor = System.Drawing.Color.Transparent;

                Label PLabel2 = new Label();
                PLabel2.Text = "Фамилия пассажира";
                PLabel2.Location = new Point(14, 100);
                PLabel2.AutoSize = true;
                PLabel2.BackColor = System.Drawing.Color.Transparent;

                Label PLabel3 = new Label();
                PLabel3.Text = "Имя пассажира";
                PLabel3.Location = new Point(14, 139);
                PLabel3.AutoSize = true;
                PLabel3.BackColor = System.Drawing.Color.Transparent;

                Label PLabel4 = new Label();
                PLabel4.Text = "Отчество пассажира";
                PLabel4.Location = new Point(14, 179);
                PLabel4.AutoSize = true;
                PLabel4.BackColor = System.Drawing.Color.Transparent;

                Label PLabel5 = new Label();
                PLabel5.Text = "Пол";
                PLabel5.Location = new Point(252, 63);
                PLabel5.AutoSize = true;
                PLabel5.BackColor = System.Drawing.Color.Transparent;

                Label PLabel6 = new Label();
                PLabel6.Text = "Возраст";
                PLabel6.Location = new Point(252, 100);
                PLabel6.AutoSize = true;
                PLabel6.BackColor = System.Drawing.Color.Transparent;


                //TextBoxes
                TextBox PText1 = new TextBox();
                PText1.Location = new Point(17, 79);
                PText1.Width = 220;
                PText1.Height = 20;
                PText1.MaxLength = 9;

                TextBox PText2 = new TextBox();
                PText2.Location = new Point(17, 116);
                PText2.Width = 220;
                PText2.Height = 20;

                TextBox PText3 = new TextBox();
                PText3.Location = new Point(17, 155);
                PText3.Width = 220;
                PText3.Height = 20;

                TextBox PText4 = new TextBox();
                PText4.Location = new Point(17, 195);
                PText4.Width = 220;
                PText4.Height = 20;

                TextBox PText5 = new TextBox();
                PText5.Location = new Point(255, 116);
                PText5.Width = 220;
                PText5.Height = 20;

                //ComboBoxes
                ComboBox PBox1 = new ComboBox();
                PBox1.Location = new Point(255, 79);
                PBox1.Width = 220;
                PBox1.Height = 20;

                //Buttons
                OkButton.Location = new Point(100, 237);
                CancelButton.Location = new Point(255, 237);

                //Form
                add.Width = 487;
                add.Height = 274;

                add.Controls.Add(PLabel1);
                add.Controls.Add(PLabel2);
                add.Controls.Add(PLabel3);
                add.Controls.Add(PLabel4);
                add.Controls.Add(PLabel5);
                add.Controls.Add(PLabel6);

                add.Controls.Add(PText1);
                add.Controls.Add(PText2);
                add.Controls.Add(PText3);
                add.Controls.Add(PText4);
                add.Controls.Add(PText5);

                add.Controls.Add(PBox1);

                string sql1 = "SELECT * FROM Gender";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    PBox1.DataSource = ds.Tables[0];
                    PBox1.DisplayMember = "Gender";
                    PBox1.ValueMember = "Gender";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string PPass = "";
                string PFname = "";
                string PName = "";
                string PSname = "";
                string PGender = "";
                string PAge = "";

                if (add.DialogResult == DialogResult.OK)
                {
                    PPass = PText1.Text;
                    PFname = PText2.Text;
                    PName = PText3.Text;
                    PSname = PText4.Text;
                    PAge = PText5.Text;
                    PGender = PBox1.SelectedValue.ToString();

                    
                }
                if (PPass != "" && PFname != "" && PName != "" && PSname != "" && PGender != "" && PAge != "")
                {
                    string sql2 = "INSERT INTO Passengers (PPassport, PFirstname, Pname, PSurname, PGender, PAge) VALUES (@PPass, @PFName, @PName, @PSname, @PGender, @PAge)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql2;
                        cmd.Parameters.AddWithValue("@PPass", PPass);
                        cmd.Parameters.AddWithValue("@PFname", PFname);
                        cmd.Parameters.AddWithValue("@PName", PName);
                        cmd.Parameters.AddWithValue("@PSname", PSname);
                        cmd.Parameters.AddWithValue("@PGender", PGender);
                        cmd.Parameters.AddWithValue("@PAge", PAge);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");
                string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                "FROM Passengers ";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];

                }


            }

            if (TTable.Checked)
            {
                //Labels
                Label TLabel1 = new Label();
                TLabel1.Text = "Пассажир";
                TLabel1.Location = new Point(14, 63);
                TLabel1.AutoSize = true;
                TLabel1.BackColor = System.Drawing.Color.Transparent;

                Label TLabel2 = new Label();
                TLabel2.Text = "Сотрудник";
                TLabel2.Location = new Point(14, 100);
                TLabel2.AutoSize = true;
                TLabel2.BackColor = System.Drawing.Color.Transparent;

                Label TLabel3 = new Label();
                TLabel3.Text = "Рейс";
                TLabel3.Location = new Point(14, 139);
                TLabel3.AutoSize = true;
                TLabel3.BackColor = System.Drawing.Color.Transparent;

                Label TLabel4 = new Label();
                TLabel4.Text = "Самолет";
                TLabel4.Location = new Point(14, 179);
                TLabel4.AutoSize = true;
                TLabel4.BackColor = System.Drawing.Color.Transparent;

                Label TLabel5 = new Label();
                TLabel5.Text = "Дата и время отправления";
                TLabel5.Location = new Point(252, 63);
                TLabel5.AutoSize = true;
                TLabel5.BackColor = System.Drawing.Color.Transparent;

                Label TLabel6 = new Label();
                TLabel6.Text = "Место";
                TLabel6.Location = new Point(252, 100);
                TLabel6.AutoSize = true;
                TLabel6.BackColor = System.Drawing.Color.Transparent;

                Label TLabel7 = new Label();
                TLabel7.Text = "Багаж";
                TLabel7.Location = new Point(252, 139);
                TLabel7.AutoSize = true;
                TLabel7.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                TextBox TText1 = new TextBox();
                TText1.Location = new Point(255, 116);
                TText1.Width = 220;
                TText1.Height = 20;

                //ComboBoxes
                ComboBox TBox1 = new ComboBox();
                TBox1.Location = new Point(17, 79);
                TBox1.Width = 220;
                TBox1.Height = 20;

                ComboBox TBox2 = new ComboBox();
                TBox2.Location = new Point(17, 116);
                TBox2.Width = 220;
                TBox2.Height = 20;

                ComboBox TBox3 = new ComboBox();
                TBox3.Location = new Point(17, 155);
                TBox3.Width = 220;
                TBox3.Height = 20;

                ComboBox TBox4 = new ComboBox();
                TBox4.Location = new Point(17, 195);
                TBox4.Width = 220;
                TBox4.Height = 20;

                //CheckBoxes
                CheckBox TChBox1 = new CheckBox();
                TChBox1.Location = new Point(255, 155);

                //DateTimePicker
                DateTimePicker TDate1 = new DateTimePicker();
                TDate1.Location = new Point(255, 79);
                TDate1.Width = 220;
                TDate1.Height = 20;

                //Buttons
                OkButton.Location = new Point(100, 237);
                CancelButton.Location = new Point(255, 237);

                //Form
                add.Width = 487;
                add.Height = 274;

                add.Controls.Add(TLabel1);
                add.Controls.Add(TLabel2);
                add.Controls.Add(TLabel3);
                add.Controls.Add(TLabel4);
                add.Controls.Add(TLabel5);
                add.Controls.Add(TLabel6);
                add.Controls.Add(TLabel7);

                add.Controls.Add(TText1);

                add.Controls.Add(TBox1);
                add.Controls.Add(TBox2);
                add.Controls.Add(TBox3);
                add.Controls.Add(TBox4);

                add.Controls.Add(TChBox1);

                add.Controls.Add(TDate1);

                string sql1 = "SELECT PPassport, CONCAT(PFirstname, ' ', SUBSTRING(PName, 1, 1), '.',SUBSTRING(PSurname, 1, 1)) AS Pas FROM Passengers";
                string sql2 = "SELECT EPassport, CONCAT(EFirstname, ' ', SUBSTRING(EName, 1, 1), '.',SUBSTRING(ESurname, 1, 1)) AS Emp FROM Employees";
                string sql3 = "SELECT FId, CONCAT(FADep, '-', FAArr) AS Fl FROM Flights";
                string sql4 = "SELECT * FROM Planes";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql1, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox1.DataSource = ds.Tables[0];
                    TBox1.DisplayMember = "Pas";
                    TBox1.ValueMember = "PPassport";


                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql2, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox2.DataSource = ds.Tables[0];
                    TBox2.DisplayMember = "Emp";
                    TBox2.ValueMember = "EPassport";
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql3, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox3.DataSource = ds.Tables[0];
                    TBox3.DisplayMember = "Fl";
                    TBox3.ValueMember = "FId";
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql4, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    TBox4.DataSource = ds.Tables[0];
                    TBox4.DisplayMember = "PLName";
                    TBox4.ValueMember = "PLName";
                }

                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);
                add.ShowDialog();

                string TPass = "";
                string TEmpl = "";
                string TFlight = "";
                DateTime TDateTime = TDate1.Value;
                string TPlane = "";
                string TSeat = "";
                bool TBaggage = false;

                if (add.DialogResult == DialogResult.OK)
                {
                    TPass = TBox1.SelectedValue.ToString();
                    TEmpl = TBox2.SelectedValue.ToString();
                    TFlight = TBox3.SelectedValue.ToString();
                    TPlane = TBox4.SelectedValue.ToString();
                    TDateTime = TDate1.Value;
                    TSeat = TText1.Text;
                    TBaggage = TChBox1.Checked;

                    
                }

                if (TPass != "")
                {
                    string sql8 = "INSERT INTO Tickets (TPass, TEmpl, TFlight, TDateTime, TPlane, TSeat, TBaggage) VALUES (@TPass, @TEmpl, @TFlight, @TDateTime, @TPlane, @TSeat, @TBaggage)";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = sql8;
                        cmd.Parameters.AddWithValue("@TPass", TPass);
                        cmd.Parameters.AddWithValue("@TEmpl", TEmpl);
                        cmd.Parameters.AddWithValue("@TFlight", TFlight);
                        cmd.Parameters.AddWithValue("@TPlane", TPlane);
                        cmd.Parameters.AddWithValue("@TDateTime", TDateTime);
                        cmd.Parameters.AddWithValue("@TSeat", TSeat);
                        cmd.Parameters.AddWithValue("@TBaggage", TBaggage);
                        cmd.ExecuteNonQuery();
                    }
                    if (TBaggage)
                    {
                        BaggageForm bf = new BaggageForm();

                        string sql5 = "SELECT * FROM Baggage";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql5, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                            bf.comboBox1.DataSource = ds.Tables[0];
                            bf.comboBox1.DisplayMember = "BType";
                            bf.comboBox1.ValueMember = "BType";
                        }

                        string sql6 = "SELECT MAX(TicketId) FROM TICKETS";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            SqlCommand com = new SqlCommand(sql6, connection);
                            bf.textBox2.Text = com.ExecuteScalar().ToString();
                        }

                        bf.ShowDialog();
                        if (bf.DialogResult == DialogResult.OK)
                        {
                            string sql7 = "INSERT INTO Registration (RTicket, RBType, RBWeight) VALUES (@RTicket, @RBType, @RBWeight)";
                            using (SqlConnection connection = new SqlConnection(connectionString))

                            {
                                connection.Open();
                                SqlCommand cmd = new SqlCommand();
                                cmd.Connection = connection;
                                cmd.CommandText = sql7;
                                cmd.Parameters.AddWithValue("@RTicket", bf.textBox2.Text);
                                cmd.Parameters.AddWithValue("@RBType", bf.comboBox1.SelectedValue);
                                cmd.Parameters.AddWithValue("@RBWeight", bf.textBox4.Text);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }
                }
                else
                    MessageBox.Show("Поле не должно быть пустым");

            }

            if (BPTable.Checked)
            {
                //Labels
                Label BPLabel1 = new Label();
                BPLabel1.Text = "Номер Билета";
                BPLabel1.Location = new Point(14, 63);
                BPLabel1.AutoSize = true;
                BPLabel1.BackColor = System.Drawing.Color.Transparent;

                //TextBoxes
                ComboBox BPBox1 = new ComboBox();
                BPBox1.Location = new Point(17, 79);
                BPBox1.Width = 220;
                BPBox1.Height = 20;

                //Buttons
                OkButton.Location = new Point(17, 116);
                CancelButton.Location = new Point(160, 116);

                //Form
                add.Width = 316;
                add.Height = 155;

                add.Controls.Add(BPLabel1);
                add.Controls.Add(BPBox1);
                add.Controls.Add(OkButton);
                add.Controls.Add(CancelButton);

                string sql2 = "SELECT TicketId FROM Tickets";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql2, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    BPBox1.DataSource = ds.Tables[0];
                    BPBox1.DisplayMember = "TicketId";
                    BPBox1.ValueMember = "TicketId";
                }

                add.ShowDialog();
                if (add.DialogResult == DialogResult.OK)
                {
                    if (BPBox1.SelectedIndex != -1)
                    {
                        string sql1 = "INSERT INTO BoardingPass (BPTId) VALUES (@BPTId)";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql1;
                            cmd.Parameters.AddWithValue("@BPTId", BPBox1.SelectedValue);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поля не должны быть пустыми!\nКоманда не была выполнена!");

                    string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(PName, 1, 1), SUBSTRING(PSurname, 1, 1)) AS 'ФИО пассажира', t.TSeat AS 'Место' " +
                "FROM BoardingPass bp " +
                "INNET JOIN Tickets t ON t.TicketId = bp.BPTId " +
                "INNER JOIN Passengers p ON p.PPassport = t.TPass";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }
                }
            }

        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (id != "")
            {
                AddUpdForm add = new AddUpdForm();
                add.AddUpdText.Text = "Обновить информацию";

                Button OkButton = new Button();
                OkButton.Width = 137;
                OkButton.Height = 23;
                OkButton.Text = "Подтвердить";
                OkButton.DialogResult = DialogResult.OK;

                Button CancelButton = new Button();
                CancelButton.Width = 137;
                CancelButton.Height = 23;
                CancelButton.Text = "Отменить";
                CancelButton.DialogResult = DialogResult.Cancel;

                if (PTable.Checked)
                {
                    //Labels
                    Label PLabel1 = new Label();
                    PLabel1.Text = "№ паспорта";
                    PLabel1.Location = new Point(14, 63);
                    PLabel1.AutoSize = true;
                    PLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel2 = new Label();
                    PLabel2.Text = "Фамилия пассажира";
                    PLabel2.Location = new Point(14, 100);
                    PLabel2.AutoSize = true;
                    PLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel3 = new Label();
                    PLabel3.Text = "Имя пассажира";
                    PLabel3.Location = new Point(14, 139);
                    PLabel3.AutoSize = true;
                    PLabel3.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel4 = new Label();
                    PLabel4.Text = "Отчество пассажира";
                    PLabel4.Location = new Point(14, 179);
                    PLabel4.AutoSize = true;
                    PLabel4.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel5 = new Label();
                    PLabel5.Text = "Пол";
                    PLabel5.Location = new Point(252, 63);
                    PLabel5.AutoSize = true;
                    PLabel5.BackColor = System.Drawing.Color.Transparent;

                    Label PLabel6 = new Label();
                    PLabel6.Text = "Возраст";
                    PLabel6.Location = new Point(252, 100);
                    PLabel6.AutoSize = true;
                    PLabel6.BackColor = System.Drawing.Color.Transparent;


                    //TextBoxes
                    TextBox PText1 = new TextBox();
                    PText1.Location = new Point(17, 79);
                    PText1.Width = 220;
                    PText1.Height = 20;
                    PText1.MaxLength = 9;

                    TextBox PText2 = new TextBox();
                    PText2.Location = new Point(17, 116);
                    PText2.Width = 220;
                    PText2.Height = 20;

                    TextBox PText3 = new TextBox();
                    PText3.Location = new Point(17, 155);
                    PText3.Width = 220;
                    PText3.Height = 20;

                    TextBox PText4 = new TextBox();
                    PText4.Location = new Point(17, 195);
                    PText4.Width = 220;
                    PText4.Height = 20;

                    TextBox PText5 = new TextBox();
                    PText5.Location = new Point(255, 116);
                    PText5.Width = 220;
                    PText5.Height = 20;

                    //ComboBoxes
                    ComboBox PBox1 = new ComboBox();
                    PBox1.Location = new Point(255, 79);
                    PBox1.Width = 220;
                    PBox1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(100, 237);
                    CancelButton.Location = new Point(255, 237);

                    //Form
                    add.Width = 487;
                    add.Height = 274;

                    add.Controls.Add(PLabel1);
                    add.Controls.Add(PLabel2);
                    add.Controls.Add(PLabel3);
                    add.Controls.Add(PLabel4);
                    add.Controls.Add(PLabel5);
                    add.Controls.Add(PLabel6);

                    add.Controls.Add(PText1);
                    add.Controls.Add(PText2);
                    add.Controls.Add(PText3);
                    add.Controls.Add(PText4);
                    add.Controls.Add(PText5);

                    add.Controls.Add(PBox1);

                    string sql1 = "SELECT * FROM Gender";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        PBox1.DataSource = ds.Tables[0];
                        PBox1.DisplayMember = "Gender";
                        PBox1.ValueMember = "Gender";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    PText1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    PText2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    PText3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    PText4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    PText5.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();

                    PBox1.SelectedValue = dataGridView1.CurrentRow.Cells[4].Value;

                    add.ShowDialog();

                    string PPass = "";
                    string PFname = "";
                    string PName = "";
                    string PSname = "";
                    string PGender = "";
                    string PAge = "";

                    if (add.DialogResult == DialogResult.OK)
                    {
                        PPass = PText1.Text;
                        PFname = PText2.Text;
                        PName = PText3.Text;
                        PSname = PText4.Text;
                        PAge = PText5.Text;
                        PGender = PBox1.SelectedValue.ToString();

                        
                    }
                    if (PPass != "" && PFname != "" && PName != "" && PSname != "" && PGender != "" && PAge != "")
                    {
                        string sql6 = "UPDATE Passengers SET PPassport = @PPass, PFirstname = @PFname, Pname = @PName, PSurname = @PSname, PGender = @PGender, PAge = @PAge WHERE PPassport = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql6;
                            cmd.Parameters.AddWithValue("@PPass", PPass);
                            cmd.Parameters.AddWithValue("@PFname", PFname);
                            cmd.Parameters.AddWithValue("@PName", PName);
                            cmd.Parameters.AddWithValue("@PSname", PSname);
                            cmd.Parameters.AddWithValue("@PGender", PGender);
                            cmd.Parameters.AddWithValue("@PAge", PAge);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                    "FROM Passengers ";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }


                }

                if (TTable.Checked)
                {
                    //Labels
                    Label TLabel1 = new Label();
                    TLabel1.Text = "Пассажир";
                    TLabel1.Location = new Point(14, 63);
                    TLabel1.AutoSize = true;
                    TLabel1.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel2 = new Label();
                    TLabel2.Text = "Сотрудник";
                    TLabel2.Location = new Point(14, 100);
                    TLabel2.AutoSize = true;
                    TLabel2.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel3 = new Label();
                    TLabel3.Text = "Рейс";
                    TLabel3.Location = new Point(14, 139);
                    TLabel3.AutoSize = true;
                    TLabel3.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel4 = new Label();
                    TLabel4.Text = "Самолет";
                    TLabel4.Location = new Point(14, 179);
                    TLabel4.AutoSize = true;
                    TLabel4.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel5 = new Label();
                    TLabel5.Text = "Дата и время отправления";
                    TLabel5.Location = new Point(252, 63);
                    TLabel5.AutoSize = true;
                    TLabel5.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel6 = new Label();
                    TLabel6.Text = "Место";
                    TLabel6.Location = new Point(252, 100);
                    TLabel6.AutoSize = true;
                    TLabel6.BackColor = System.Drawing.Color.Transparent;

                    Label TLabel7 = new Label();
                    TLabel7.Text = "Багаж";
                    TLabel7.Location = new Point(252, 139);
                    TLabel7.AutoSize = true;
                    TLabel7.BackColor = System.Drawing.Color.Transparent;

                    //TextBoxes
                    TextBox TText1 = new TextBox();
                    TText1.Location = new Point(255, 116);
                    TText1.Width = 220;
                    TText1.Height = 20;

                    //ComboBoxes
                    ComboBox TBox1 = new ComboBox();
                    TBox1.Location = new Point(17, 79);
                    TBox1.Width = 220;
                    TBox1.Height = 20;

                    ComboBox TBox2 = new ComboBox();
                    TBox2.Location = new Point(17, 116);
                    TBox2.Width = 220;
                    TBox2.Height = 20;

                    ComboBox TBox3 = new ComboBox();
                    TBox3.Location = new Point(17, 155);
                    TBox3.Width = 220;
                    TBox3.Height = 20;

                    ComboBox TBox4 = new ComboBox();
                    TBox4.Location = new Point(17, 195);
                    TBox4.Width = 220;
                    TBox4.Height = 20;

                    //CheckBoxes
                    CheckBox TChBox1 = new CheckBox();
                    TChBox1.Location = new Point(255, 155);

                    //DateTimePicker
                    DateTimePicker TDate1 = new DateTimePicker();
                    TDate1.Location = new Point(255, 79);
                    TDate1.Width = 220;
                    TDate1.Height = 20;

                    //Buttons
                    OkButton.Location = new Point(100, 237);
                    CancelButton.Location = new Point(255, 237);

                    //Form
                    add.Width = 487;
                    add.Height = 274;

                    add.Controls.Add(TLabel1);
                    add.Controls.Add(TLabel2);
                    add.Controls.Add(TLabel3);
                    add.Controls.Add(TLabel4);
                    add.Controls.Add(TLabel5);
                    add.Controls.Add(TLabel6);
                    add.Controls.Add(TLabel7);

                    add.Controls.Add(TText1);

                    add.Controls.Add(TBox1);
                    add.Controls.Add(TBox2);
                    add.Controls.Add(TBox3);
                    add.Controls.Add(TBox4);

                    add.Controls.Add(TChBox1);

                    add.Controls.Add(TDate1);

                    string sql1 = "SELECT PPassport, CONCAT(PFirstname, ' ', SUBSTRING(PName, 1, 1), '.',SUBSTRING(PSurname, 1, 1)) AS Pas FROM Passengers";
                    string sql2 = "SELECT EPassport, CONCAT(EFirstname, ' ', SUBSTRING(EName, 1, 1), '.',SUBSTRING(ESurname, 1, 1)) AS Emp FROM Employees";
                    string sql3 = "SELECT FId, CONCAT(FADep, '-', FAArr) AS Fl FROM Flights";
                    string sql4 = "SELECT * FROM Planes";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql1, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox1.DataSource = ds.Tables[0];
                        TBox1.DisplayMember = "Pas";
                        TBox1.ValueMember = "PPassport";


                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql2, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox2.DataSource = ds.Tables[0];
                        TBox2.DisplayMember = "Emp";
                        TBox2.ValueMember = "EPassport";
                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql3, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox3.DataSource = ds.Tables[0];
                        TBox3.DisplayMember = "Fl";
                        TBox3.ValueMember = "FId";
                    }
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql4, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);
                        TBox4.DataSource = ds.Tables[0];
                        TBox4.DisplayMember = "PLName";
                        TBox4.ValueMember = "PLName";
                    }

                    add.Controls.Add(OkButton);
                    add.Controls.Add(CancelButton);

                    TBox1.SelectedValue = dataGridView1.CurrentRow.Cells[1].Value;
                    TBox2.SelectedValue = dataGridView1.CurrentRow.Cells[2].Value;
                    TBox3.SelectedValue = dataGridView1.CurrentRow.Cells[3].Value;
                    TBox4.SelectedValue = dataGridView1.CurrentRow.Cells[5].Value;

                    TDate1.Value = (DateTime)dataGridView1.CurrentRow.Cells[4].Value;

                    TText1.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();

                    TChBox1.Checked = (bool)dataGridView1.CurrentRow.Cells[7].Value;

                    add.ShowDialog();

                    string TPass = "";
                    string TEmpl = "";
                    string TFlight = "";
                    DateTime TDateTime = TDate1.Value;
                    string TPlane = "";
                    string TSeat = "";
                    bool TBaggage = false;

                    if (add.DialogResult == DialogResult.OK)
                    {
                        TPass = TBox1.SelectedValue.ToString();
                        TEmpl = TBox2.SelectedValue.ToString();
                        TFlight = TBox3.SelectedValue.ToString();
                        TPlane = TBox4.SelectedValue.ToString();
                        TDateTime = TDate1.Value;
                        TSeat = TText1.Text;
                        TBaggage = TChBox1.Checked;

                        
                    }

                    if (TPass != "")
                    {
                        string sql5 = "UPDATE Tickets SET TPass = @TPass, TEmpl = @TEmpl, TFlight = @TFlight, TDateTime = @TDateTime, TPlane = @TPlane, TSeat = @TSeat, TBaggage = @TBaggage WHERE TicketID = @Id";
                        using (SqlConnection connection = new SqlConnection(connectionString))

                        {
                            connection.Open();
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = sql5;
                            cmd.Parameters.AddWithValue("@TPass", TPass);
                            cmd.Parameters.AddWithValue("@TEmpl", TEmpl);
                            cmd.Parameters.AddWithValue("@TFlight", TFlight);
                            cmd.Parameters.AddWithValue("@TPlane", TPlane);
                            cmd.Parameters.AddWithValue("@TDateTime", TDateTime);
                            cmd.Parameters.AddWithValue("@TSeat", TSeat);
                            cmd.Parameters.AddWithValue("@TBaggage", TBaggage);
                            cmd.Parameters.AddWithValue("@Id", id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                        MessageBox.Show("Поле не должно быть пустым");
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];

                    }

                }
            }
            else
                MessageBox.Show("Выберите элемент базы данных");
        }

        private void Search_TextChanged(object sender, EventArgs e)
        {

            if (BPTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(PName, 1, 1), SUBSTRING(PSurname, 1, 1)) AS 'ФИО пассажира', t.TSeat AS 'Место' " +
                "FROM BoardingPass bp " +
                "INNER JOIN Tickets t ON t.TicketId = bp.BPTId " +
                "INNER JOIN Passengers p ON p.PPassport = t.TPass" +
                    "WHERE BPId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT BPId AS 'Номер пасадочного талона', BPTId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(PName, 1, 1), SUBSTRING(PSurname, 1, 1)) AS 'ФИО пассажира', t.TSeat AS 'Место' " +
               "FROM BoardingPass bp " +
               "INNET JOIN Tickets t ON t.TicketId = bp.BPTId " +
               "INNER JOIN Passengers p ON p.PPassport = t.TPass" +
                    "WHERE BPTId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }

            if (PTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PPassport LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter2.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PFirstname LIKE CONCAT(@Search, '%') OR PName LIKE CONCAT('%', @Search, '%') OR PSurname LIKE CONCAT('%', @Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com); adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter3.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PGender LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

                if (Filter4.Checked)
                {
                    string sql = "SELECT PPassport AS 'Номер паспорта', PFirstname AS 'Фамилия', PName AS 'Имя', PSurname AS 'Отчество', PGender AS 'Пол', PAge AS 'Возраст' " +
                                    "FROM Passengers " +
                                    "WHERE PAge LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }

            if (RTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                    "FROM Registration " +
                    "WHERE RId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter2.Checked)
                {
                    string sql = "SELECT RId AS 'Номер багажного талона', RTicket AS 'Номер билета', RBType AS 'Тип багажа', RBWeight AS 'Вес багажа' " +
                    "FROM Registration " +
                    "WHERE RTicket LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }

            if (TTable.Checked)
            {
                if (Filter1.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl" +
                    "WHERE TicketId LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter2.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                 "FROM Tickets " +
                 "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                 "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl" +
                    "WHERE p.PFirstname LIKE CONCAT(@Search, '%') OR p.PName LIKE CONCAT('%',@Search,'%') OR p.PSurname LIKE CONCAT('%',@Search,'%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter3.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                "FROM Tickets " +
                "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl" +
                    "WHERE e.EFirstname LIKE CONCAT(@Search, '%') OR e.EName LIKE CONCAT('%',@Search,'%') OR e.ESurname LIKE CONCAT('%',@Search,'%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter4.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                 "FROM Tickets " +
                 "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                 "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl" +
                    "WHERE TFlight LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }
                if (Filter5.Checked)
                {
                    string sql = "SELECT TicketId AS 'Номер билета', CONCAT(p.PFirstname, SUBSTRING(p.PName, 1, 1), SUBSTRING(p.PSurname, 1, 1)) AS 'ФИО пассажира', CONCAT(e.EFirstname, SUBSTRING(e.EName, 1, 1), SUBSTRING(e.ESurname, 1, 1)) AS 'ФИО сотрудника', TFlight AS 'Рейс №', TDateTime AS 'Дата отправления', TPlane AS 'Самолет', TSeat AS 'Место', TBaggage AS 'Багаж' " +
                 "FROM Tickets " +
                 "INNER JOIN Passengers p ON p.PPassport = Tickets.TPass " +
                 "INNER JOIN Employees e ON e.EPassport = Tickets.TEmpl" +
                    "WHERE TDateTime LIKE CONCAT(@Search, '%')";
                    using (SqlConnection connection = new SqlConnection(connectionString))

                    {
                        connection.Open();
                        SqlCommand com = new SqlCommand(sql, connection);
                        com.Parameters.AddWithValue("@Search", Search.Text);
                        adapter = new SqlDataAdapter(com);

                        ds = new DataSet();
                        adapter.Fill(ds);
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Выберите объект таблицы");
            }
        }
    }
}
